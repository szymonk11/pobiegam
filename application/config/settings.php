<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['settings'] = array(
    'per_page' => 10,
    'last_posts' => 10,
    'email' => 'szymonk1101@gmail.com',
    'secret_key' => '6LcBsDIUAAAAAB228AItTosHZcegO_sU3r1SImGo',
    //TEMPLATE
    'title' => 'Pobiegam.NET - Strona główna',
    'main_view' => 'index',
    'web_dir' => 'web/',
    'css_dir' => 'web/css/',
    'js_dir' => 'web/js/',
    'img_dir' => 'web/img/',
    'admin_dir' => 'web/admin/'
);
