<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->model('posts_model');

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Content-Type: application/json');
    }
	
	public function get_types()
	{
		$ret = new stdClass();
		$ret->status = 0;
		
		$ret->data = get_all_types();
		$ret->status = 1;
		
		echo json_encode($ret, JSON_NUMERIC_CHECK);
		die();
	}
    
    public function get_post()
    {
        $ret = new stdClass();
		$ret->status = 0;
        $ret->message = 'Niepoprawne dane.';
        
        $post_id = $this->input->post('postid');

        if(!$post_id) {
            echo json_encode($ret, JSON_NUMERIC_CHECK);
            die();
        }

        $post = $this->posts_model->getPostDetails($post_id);

        if(!$post || empty($post) || $post->visible != POST_VISIBLE) {
            $ret->message = 'Nie odnaleziono wpisu.';
            echo json_encode($ret, JSON_NUMERIC_CHECK);
            die();
        }
		
		$post->type_name = get_name_by_type($post->type);
		$post->img_name = get_img_name_by_type($post->type);
       
        $ret->status = 1;
        $ret->message = 'Ok';
        $ret->data = $post;
        echo json_encode($ret, JSON_NUMERIC_CHECK);
        die();
    }

	public function get_posts()
    {
        $ret = new stdClass();

        $page = $this->input->post_get('page');
        if(!$page) $page = 1;

        $type = $this->input->post_get('type');
        if(!$type) $type = '';
        
        $city = $this->input->post_get('city');
        if(!$city) $city = '';
		
		$per_page = $this->input->post_get('per_page');
		if(!$per_page) $per_page = NULL;

        $posts = $this->posts_model->getSearchPosts($page, $type, $city, $per_page);
	    //$max_pages = $this->posts_model->getSearchMaxPages($type, $city);
	
        foreach($posts as $post) {
			$post->type_name = get_name_by_type($post->type);
			$post->img_name = get_img_name_by_type($post->type);
        }
        
        $ret->data = $posts;
        $ret->max_pages = $this->posts_model->getSearchMaxPages($type, $city, $per_page);

        echo json_encode($ret, JSON_NUMERIC_CHECK);
        die();
    }
	
    public function get_last_posts()
    {
        $posts = $this->posts_model->getLastPosts();

        foreach($posts as $post) {
			$post->type_name = get_name_by_type($post->type);
			$post->img_name = get_img_name_by_type($post->type);
		}

        echo json_encode($posts, JSON_NUMERIC_CHECK);
        die();
    }
	
	public function add_post()
	{
		$ret = new stdClass();
		$ret->status = 0;
		$ret->message = 'Nieprawidłowe dane.';
		
		$validation_rules = $this->posts_model->getValidationRules();
        $validation_errors = FALSE;
        $date_error = FALSE;
        $recaptcha_error = FALSE;

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules($validation_rules);

        $postData = $this->input->post();

        if(!empty($postData)) {
			
			//fix for mobile checkbox
			$postData['reg'] = intval(array_shift($postData['reg']));
			$postData['marketing'] = intval(array_shift($postData['marketing']));
			
            if(!isset($postData['day']) || empty($postData['day'])) {
                if(!isset($postData['date']) || empty($postData['date'])) {
                    $validation_errors = TRUE;
                    $date_error = 'Musisz wybrać dni lub datę, w której chcesz ćwiczyć.';
                }
            } else {
                $postData['date'] = implode(', ', $postData['day']);
            }

            /*if(!isset($postData['g-recaptcha-response']))
                $postData['g-recaptcha-response'] = '';
                
            $SECRET_KEY = $this->config->item('secret_key');

            $recaptcha_check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$SECRET_KEY.'&response='.$postData['g-recaptcha-response']);
            $recaptcha_checked = json_decode($recaptcha_check);
            if($recaptcha_checked->success != true) {
                $validation_errors = TRUE;
                $recaptcha_error = 'Potwierdź, że nie jest botem.';
            }*/
        }

		$this->form_validation->set_data($postData);

        if($this->form_validation->run() != FALSE) {
  
            $postData['time'] = $postData['time_from'].' - '.$postData['time_to'];

            $postData['telephone'] = substr(trim((string)$postData['telephone']), 0, 32);
            if($postData['telephone'] == '')
                $postData['telephone'] = '-';

            $postData['description'] = substr(trim((string)$postData['description']), 0, 512);
            if($postData['description'] == '')
                $postData['description'] = '---';    

            $postData['unikey'] = uniqid('', true);
            $postData['expire'] = time() + (21 * 24 * 60 * 60);

            if(!$validation_errors) {

                $accept_type = $this->config->item('posts_accept_type');

                if($accept_type == 'mail')
                    $postData['visible'] = POST_INVISIBLE;

                $added = $this->posts_model->addPost($postData);
                
                if($added == true) {
                      
                    $expire = date('d-m-Y', $postData['expire']);
                    $this->load->library('Mailing');
                    if($accept_type == 'mail') {
                        $this->mailing->send_add_post_accept_msg($postData['email'], $postData['name'], $postData['unikey'], $expire);
                        
						$ret->message = 'Ogłoszenie zostało pomyślnie dodane. Sprawdź teraz swoją skrzynkę pocztową, aby dokończyć proces.';
						
                    } else {
                        $this->mailing->send_add_post_msg($postData['email'], $postData['name'], $postData['unikey'], $expire);
						
						$ret->message = 'Ogłoszenie zostało pomyślnie dodane.';
                    }
					
                    $ret->status = 1;
					echo json_encode($ret, JSON_NUMERIC_CHECK);
                    die();
                    
                } else {
					$ret->message = 'Wystąpił nieoczekiwany błąd. Spróbuj jeszcze raz...';
					echo json_encode($ret, JSON_NUMERIC_CHECK);
                    die();
                }
            }
        } else {
			
			$ret->message = $this->form_validation->error_string(' ', '<br />');
			echo json_encode($ret, JSON_NUMERIC_CHECK);
			die(); 
        }
	
		if($date_error)
			$ret->message = $date_error;
		
		echo json_encode($ret, JSON_NUMERIC_CHECK);
		die(); 
	}

}
