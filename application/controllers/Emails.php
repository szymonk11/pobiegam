<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
    }
    
    public function contact()
    {
        $this->load->view('emails/contact', $this->input->get());
    }

    public function add_post()
    {
        $this->load->view('emails/add_post', $this->input->get());
    }

    public function add_post_accept()
    {
        $this->load->view('emails/add_post_accept', $this->input->get());
    }

}
