<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('posts_model');

		$posts = $this->posts_model->getLastPosts();

		$this->load->view('layout', array(
			'view' => 'index',
			'title' => 'Pobiegam.NET - Portal łączący ludzi z pasją! • Najnowsze ogłoszenia',
			'posts' => $posts
		));
	}

	public function terms()
	{
		$this->load->view('layout', array(
			'view' => 'pages/terms',
			'title' => 'Regulamin • Pobiegam.NET - Portal łączący ludzi z pasją!'
		));
	}

	public function about()
	{
		$this->load->view('layout', array(
			'view' => 'pages/about',
			'title' => 'O projekcie Pobiegam.NET • Jak to działa? • O co chodzi?'
		));
	}

	public function contact()
	{
		$this->load->view('layout', array(
			'view' => 'pages/contact',
			'title' => 'Kontakt z Pobiegam.NET • Napisz do nas! • Współpraca, pytania, problemy, propozycje'
		));
	}

	public function contact_json()
	{
		header('Content-Type: application/json');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'imię', 'required|min_length[3]');
		$this->form_validation->set_rules('email', 'e-mail', 'required|valid_email');
		$this->form_validation->set_rules('text', 'treść', 'required|min_length[3]');
		$this->form_validation->set_rules('recaptcha', 'recaptcha', 'required');

		if($this->form_validation->run() != FALSE) {

			$validation_errors = FALSE;
			$SECRET_KEY = $this->config->item('secret_key');
			$recaptcha = $this->input->post('recaptcha');

            $recaptcha_check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$SECRET_KEY.'&response='.$recaptcha);
           	$recaptcha_checked = json_decode($recaptcha_check);
            if($recaptcha_checked->success != true) {
                $validation_errors = TRUE;
                $recaptcha_error = 'Potwierdź, że nie jest botem.';
			}
			
			if(!$validation_errors) {

				$this->load->library('Mailing');
				$this->mailing->send_contact_msg($this->input->post('email'), $this->input->post('text'), $this->input->post('name'));

				die(json_encode(array('status' => 'success')));
			}
		}

		die(json_encode(array('status' => 'error')));
	}

	public function error($type = 404)
	{
		if($type == 404) {

			$this->load->view('layout', array(
				'view' => 'errors/404',
				'title' => 'Nie znaleziono strony • Pobiegam.NET - Portal łączący ludzi z pasją!'
			));

		}
	}
}

