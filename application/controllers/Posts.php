<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('posts_model');
	}

	public function index($page = 1)
	{
        $posts = $this->posts_model->getPosts($page);
        $max_pages = $this->posts_model->getMaxPages();

        $this->load->view('layout', array(
			'view' => 'posts/index',
			'title' => 'Szukaj osób • Lista ogłoszeń strona '.$page.' • Pobiegam.NET - Portal łączący ludzi z pasją!',
			'posts' => $posts,
            'active_page' => $page,
            'max_pages' => $max_pages
		));
    }
    
    public function post($post_id, $name = '')
    {
        $post = $this->posts_model->getPostDetails($post_id);

        if(!$post || empty($post) || $post->visible != POST_VISIBLE)
            show_404();

        $title = get_name_by_type($post->type).' • '.$post->name.' ('.$post->age.') • '.$post->city.' • Pobiegam.NET - Portal łączący ludzi z pasją!';
        
        $this->load->library('user_agent');
        $back_url = $this->agent->referrer();
        if(!$this->agent->referrer() || $this->agent->referrer() == base_url(uri_string())) {
            $back_url = base_url('posts');
        }
       
        TemplateManager::add_css('web/js/themes/bars-movie.min.css');
        $this->load->view('layout', array(
			'view' => 'posts/post',
			'title' =>  $title,
            'post' => $post,
            'back_url' => $back_url
		));
    }

    public function add()
    {
        $validation_rules = $this->posts_model->getValidationRules();
        $validation_errors = FALSE;
        $date_error = FALSE;
        $recaptcha_error = FALSE;

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules($validation_rules);

        $postData = $this->input->post();

        if(!empty($postData)) {

            if(!isset($postData['day']) || empty($postData['day'])) {
                if(!isset($postData['date']) || empty($postData['date'])) {
                    $validation_errors = TRUE;
                    $date_error = 'Musisz wybrać dni lub datę, w której chcesz ćwiczyć.';
                }
            } else {
                $postData['date'] = implode(', ', $postData['day']);
            }

            if(!isset($postData['g-recaptcha-response']))
                $postData['g-recaptcha-response'] = '';
                
            $SECRET_KEY = $this->config->item('secret_key');

            $recaptcha_check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$SECRET_KEY.'&response='.$postData['g-recaptcha-response']);
            $recaptcha_checked = json_decode($recaptcha_check);
            if($recaptcha_checked->success != true) {
                $validation_errors = TRUE;
                $recaptcha_error = 'Potwierdź, że nie jest botem.';
            }
        }


        if($this->form_validation->run() != FALSE) {
  
            $postData['time'] = $postData['time_from'].' - '.$postData['time_to'];

            $postData['telephone'] = substr(trim((string)$postData['telephone']), 0, 32);
            if($postData['telephone'] == '')
                $postData['telephone'] = '-';

            $postData['description'] = substr(trim((string)$postData['description']), 0, 512);
            if($postData['description'] == '')
                $postData['description'] = '---';    

            $postData['unikey'] = uniqid('', true);
            $postData['expire'] = time() + (21 * 24 * 60 * 60);

            if(!$validation_errors) {

                $accept_type = $this->config->item('posts_accept_type');

                if($accept_type == 'mail')
                    $postData['visible'] = POST_INVISIBLE;

                $added = $this->posts_model->addPost($postData);
                
                if($added == true) {
                      
                    $expire = date('d-m-Y', $postData['expire']);
                    $this->load->library('Mailing');
                    if($accept_type == 'mail') {
                        $this->mailing->send_add_post_accept_msg($postData['email'], $postData['name'], $postData['unikey'], $expire);
                        $this->session->set_flashdata('message_success', 'Ogłoszenie zostało pomyślnie dodane. Sprawdź teraz swoją skrzynkę pocztową, aby dokończyć proces.');
                    } else {
                        $this->mailing->send_add_post_msg($postData['email'], $postData['name'], $postData['unikey'], $expire);
                        $this->session->set_flashdata('message_success', 'Ogłoszenie zostało pomyślnie dodane.');
                    }
                    
                    redirect('');
                    
                } else {
                    $this->session->set_flashdata('message_error', 'Wystąpił nieoczekiwany błąd. Spróbuj jeszcze raz...');
                }
            }
        } else {
            if($this->form_validation->error_string())
                $validation_errors = TRUE;  
        }

        TemplateManager::add_css('web/js/themes/bars-movie.min.css');
        $this->load->view('layout', array(
			'view' => 'posts/add',
            'title' =>  'Dodaj ogłoszenie na Pobiegam.NET',
            'errors' => $validation_errors,
            'date_error' => $date_error,
            'recaptcha_error' => $recaptcha_error
		));
    }

    public function search()
    {
        $page = $this->input->get('page');
        if(!$page) $page = 1;

        $type = $this->input->get('type');
        if(!$type) $type = '';
        
        $city = $this->input->get('city');
        if(!$city) $city = '';

        $posts = $this->posts_model->getSearchPosts($page, $type, $city);
	    $max_pages = $this->posts_model->getSearchMaxPages($type, $city);
	
        $search_data = array(
            'type' => $type,
            'city' => $city
        );
    
        $this->load->view('layout', array(
			'view' => 'posts/search',
            'title' =>  'Szukaj osób • lista ogłoszeń strona '.$page.' • Pobiegam.NET - Portal łączący ludzi z pasją!',
            'posts' => $posts,
            'active_page' => $page,
            'max_pages' => $max_pages,
            'search_data' => $search_data
		));
    }

    public function delete()
    {
        $unikey = $this->input->post_get('unikey');
        $email = $this->input->post_get('email');
        
        if(!empty($unikey) && !empty($email)) {

            $post = $this->posts_model->getPostByUnikeyEmail($unikey, $email);

            if($post) {

                $confirm = $this->input->post_get('confirm');

                if(!empty($confirm)) {

                    $this->posts_model->delete($post->id);

                    $this->session->set_flashdata('message_success', 'Ogłoszenie zostało usunięte.');
                    redirect('');

                    return;

                } else {

                    $this->load->view('layout', array(
                        'view' => 'posts/delete',
                        'title' =>  'Usuwanie ogłoszenia • Pobiegam.NET - Portal łączący ludzi z pasją!',
                        'unikey' => $unikey,
                        'email' => urlencode($email)
                    ));

                    return;
                }
            }
        }

        $this->load->view('layout', array(
            'view' => 'posts/delete',
            'title' =>  'Usuwanie ogłoszenia • Pobiegam.NET - Portal łączący ludzi z pasją!',
            'message_error' => 'Wystąpił błąd. Brak danych...'
        ));
    }

    public function accept()
    {
        $unikey = $this->input->post_get('unikey');
        $email = $this->input->post_get('email');
        
        if(!empty($unikey) && !empty($email)) {

            $post = $this->posts_model->getPostByUnikeyEmail($unikey, $email);

            if($post) {

                $this->posts_model->accept($post->id);

                $this->session->set_flashdata('message_success', 'Ogłoszenie zostało zaakceptowane i za chwilę pojawi się w serwisie.');
                redirect('');

            }
        }

        $this->session->set_flashdata('message_error', 'Ups... coś poszło nie tak. Przepraszamy!');
        redirect('');
    }

    /* AJAX REPORT POST */
    public function report($post_id)
    {
        header('Content-Type: application/json');

        $idd = $this->input->post('id');
        $reason = $this->input->post('reason');
        
        if(isset($idd) && !empty($idd) && $idd == $post_id) {

            $this->load->model('reports_model');
            
            $report = array(
                'post_id' => $idd,
                'status' => 1,
                'ip' => $this->input->ip_address(),
                'desc' => $reason
            );
            
            if($this->reports_model->addReport($report)) {
                die(json_encode(array('status' => 1), JSON_NUMERIC_CHECK));
            } else {
                die(json_encode(array('status' => 0), JSON_NUMERIC_CHECK));
            }

        }

        die(json_encode(array('status' => 0), JSON_NUMERIC_CHECK));

    }
}

