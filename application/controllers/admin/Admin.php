<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    const SETTINGS_FILE = 'settings';
    protected $settings;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->config(self::SETTINGS_FILE);
        $this->settings = $this->config->item('settings');
        
        $this->load->helper('pobiegam');


        //$this->template->layout = 'admin/layout';
        //$this->template->active_page = $this->uri->segment(2, 'index');
    }
    
    public function index()
    {
        if($this->session->logged_in != 1 && $this->session->admin != 5) {
            redirect('/pa/login');
            die();
        }
            
        $this->load->model('posts_model');
        $this->load->model('users_model');
	    $this->load->model('reports_model');
        $posts_count = $this->posts_model->getPostsCount();
        $users_count = $this->users_model->get_count_users();
        $reports_count = $this->reports_model->getReportsCount();
        
        $this->load->view('admin/layout', array(
			'view' => 'admin/index',
			'posts_count' => $posts_count,
            'users_count' => $users_count,
	        'reports_count' => $reports_count
		));
    }
    
    public function login()
    {
        $action = $this->input->get('action');
        $email = $this->input->post('email');
        
        if(isset($email)) {
            
            $password = $this->input->post('password');
            
            $this->load->model('users_model');
            
            $user = $this->users_model->login($email, $password);
            
            if($user) {        
                $this->session->set_userdata(array(
                    'email' => $email,
                    'name' => $user['name'],
                    'admin' => $user['admin'],
                    'rank' => $user['rank'],
		            'registered' => $user['registered_date'],
                    'logged_in' => 1
                ));
                redirect('/pa/');

            } else {
                $this->load->view('admin/login', array(
                    'error' => true
                ));
            }
        } else {
            $this->load->view('admin/login', array(
                'logout' => $action
            ));
        }
    }
    
    public function logout()
    {
        $this->session->unset_userdata(array('email', 'name', 'logged_in', 'admin'));
        $this->session->sess_destroy();
        redirect('/pa/login/?action=logout');
    }
    
    public function ogloszenia()
    {
        if($this->session->logged_in != 1 && $this->session->admin != 5) {
            redirect('/pa/login');
            die();
        }
	
        $error = $this->input->get('error');
        if(isset($error)) {
            if($error == 'not_found') {
                $error = 'Nie znaleziono wybranego ogłoszenia.';
            }
            if($error == 'not_delete') {
                $error = 'Nieoczekiwany błąd poczas usuwania.';
	        }
	    }
	
        $success = $this->input->get('success');
        if(isset($success)) {
            if($success == 'update') {
                $success = 'Informacje o ogłoszeniu zostały zaktualizowane.';
            }
            if($success == 'delete') {
                $success = 'Ogłoszenie zostało usunięte.';
            }
        }
        
        $this->load->model('posts_model');
        $posts = $this->posts_model->getAllPosts(FALSE);
        
        $this->load->view('admin/layout', array(
            'view' => 'admin/ogloszenia',
			'posts' => $posts,
            'error' => $error,
            'success' => $success
        ));
    }
    
    public function raporty() {
        if($this->session->logged_in != 1 && $this->session->admin != 5) {
            redirect('/pa/login');
            die();
        }
        
        $this->load->model('reports_model');
        
        $reports = $this->reports_model->getAllReports();
        
        $success = $this->input->get('success');
        $error = $this->input->get('error');
        
        $this->load->view('admin/layout', array(
            'view' => 'admin/raporty',
			'reports' => $reports,
            'success' => $success,
            'error' => $error
        ));
    }
}

