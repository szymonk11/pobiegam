<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPosts extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		if($this->session->logged_in != 1 && $this->session->admin != 5) {
			redirect('/pa/login');
			die();
		}
		
		$this->load->model('posts_model');
		$this->load->helper('pobiegam');
    }
    
	public function edytuj($id)
	{
		$error = FALSE;
		$sport = $this->input->post('sport');
		
		if(isset($sport)) {
			
			$post = array();
			$post['type'] = $sport;
			$post['level'] = $this->input->post('level');
			$post['city'] = $this->input->post('city');
			$post['date'] = $this->input->post('date');
			$post['time'] = sprintf('%s - %s', $this->input->post('timefrom'), $this->input->post('timeto'));
			$post['name'] = $this->input->post('name');
			$post['age'] = $this->input->post('age');
			$post['email'] = $this->input->post('email');    
			$post['telephone'] = $this->input->post('tel');
			if(!isset($post['telephone']) || $post['telephone'] == '') { $post['telephone'] = '-'; }
			$post['gg'] = $this->input->post('gg');
			if(!isset($post['gg']) || $post['gg'] == '') { $post['gg'] = '-'; }
			$post['desc'] = $this->input->post('desc');
			if(!isset($post['desc']) || $post['desc'] == '') { $post['desc'] = '---'; }
			$post['expire'] = strtotime($this->input->post('expire'));
			if(!$post['expire'] || $post['expire'] <= time()) {
				$error = 'Sprawdź poprawność daty wygaśnięcia ogłoszenia!';
			}
			
			if(!$error) {
				if($update = $this->PostsModel->update($id, $post)) {
					redirect('/pa/ogloszenia/?success=update');
				} else {
					$error = 'Wystąpił nieoczekiwany błąd. Sprawdź poprawność wprowadzonych danych i spróbuj jeszcze raz...';
				}
			}
		}
		
		$post = $this->posts_model->getPostDetails($id);
		
		if(!$post) {
			redirect('/pa/ogloszenia?error=not_found');
		}

		TemplateManager::add_javascript('web/js/jquery-ui.min.js');
		TemplateManager::add_javascript('web/js/jquery.timepicker.min.js');
		TemplateManager::add_javascript('web/admin/js/edit_post.js');
		TemplateManager::add_css('web/css/jquery-ui.min.css');

		$this->load->view('admin/layout', array(
            'view' => 'admin/edytuj',
			'sports' => get_all_sports(),
			'post' => $post,
			'error' => $error
        ));
	}
    
	public function usun($id)
	{
		$post = $this->posts_model->getPostDetails($id);
		if(!$post) {
			redirect('pa/ogloszenia?error=not_found');
		}

		if($this->posts_model->delete($id, FALSE))
			redirect('pa/ogloszenia?success=delete');
		else
			redirect('pa/ogloszenia?error=not_delete');
	}
	
	public function accept($id)
	{
		$post = $this->posts_model->getPostDetails($id);
		if(!$post) {
			redirect('pa/ogloszenia?error=not_found');
		}

		if($this->posts_model->accept($id))
			redirect('pa/ogloszenia?success=accept');
		else
			redirect('pa/ogloszenia?error=not_accept');
	}
}

