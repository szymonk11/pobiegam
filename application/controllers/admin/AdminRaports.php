<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminRaports extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        if($this->session->logged_in != 1 && $this->session->admin != 5) {
            redirect('pa/login');
            die();
        }
        
        $this->load->model('reports_model');
    }
    
    public function raporty($id) {
        
        $action = $this->input->get('action');
        if(isset($action)) {
                        
            $status = array(
                'nowy' => 1,
                'start' => 2,
                'zakoncz' => 3,
                'anuluj' => 4
            );
            
            if(isset($status[$action])) {
                if($this->reports_model->changeStatus($id, $status[$action])) {
                    redirect('pa/raporty?success='.urlencode('Status zgłoszenia o ID '.$id.' został zmieniony.'));
                    return true;
                }
            }
        }
        redirect('pa/raporty?error='.urlencode('Wystąpił nieoczekiwany błąd.'));
        return false;
    }
    
}

