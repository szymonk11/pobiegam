<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('get_post_url')) {

    function get_post_url($post)
    {
        $arg1 = array('ą', 'Ą', 'ć', 'Ć', 'ę', 'Ę', 'ł', 'Ł', 'ń', 'Ń', 'ó', 'Ó', 'ś', 'Ś', 'ź', 'Ź', 'ż', 'Ż' );
        $arg2 = array('a', 'a', 'c', 'c', 'e', 'e', 'l', 'l', 'n', 'n', 'o', 'o', 's', 's', 'z', 'z', 'z', 'z' );
        
        $third = str_replace($arg1, $arg2, get_name_by_type($post->type).'-'.$post->city.'-'.$post->name);
        
        return base_url('post/'.$post->id.'/'.urlencode(str_replace(array(' ','/'), '-', $third)));
    }
}

if(!function_exists('get_all_sports')) {

    function get_all_sports()
    {
        $sports = array(
            0 => array('name' => 'Wszystko', 'img' => 'run.png'),
            1 => array('name' => 'Bieganie', 'img' => 'run.png'),
            2 => array('name' => 'Rower', 'img' => 'biker.png'),
            3 => array('name' => 'Tenis', 'img' => 'tenis.png'),
            4 => array('name' => 'Nordic Walking', 'img' => 'nordic.png'),
            5 => array('name' => 'Piłka nożna', 'img' => 'football.png'),
            6 => array('name' => 'Koszykówka', 'img' => 'basketball.png'),
            7 => array('name' => 'Siłownia/Fitness', 'img' => 'gym.png'),
            8 => array('name' => 'Siatkówka', 'img' => 'volleyball.png'),
            9 => array('name' => 'Kręgle/Bilard', 'img' => 'bilard.png'),
            10 => array('name' => 'Pływanie', 'img' => 'swimming.png'),
            11 => array('name' => 'Inne', 'img' => 'run.png')
        );

        return $sports;
    }
}

if(!function_exists('get_name_by_type')) {

    function get_name_by_type($type)
    {
        $name = '';
        switch($type) 
        {
            case 0:
                $name = 'Wszystko';
                break;
            case 1:
                $name = 'Bieganie';
                break;
            case 2:
                $name = 'Rower';
                break;
            case 3:
                $name = 'Tenis';
                break;
            case 4:
                $name = 'Nordic Walking';
                break;
            case 5:
                $name = 'Piłka nożna';
                break;
            case 6:
                $name = 'Koszykówka';
                break;
            case 7:
                $name = 'Siłownia/Fitness';
                break;
            case 8:
                $name = 'Siatkówka';
                break;
            case 9:
                $name = 'Kręgle/Bilard';
                break;
            case 10:
                $name = 'Pływanie';
                break;
            case 11:
                $name = 'Inne';
                break;
            default:
                $name = '';
                break;
        }

        return $name;
    }
}

if(!function_exists('get_img_name_by_type')) {

    function get_img_name_by_type($type)
    {
        $img = '';
        switch($type) 
        {
            case 0:
                $img = 'run.png';
                break;
            case 1:
                $img = 'run.png';
                break;
            case 2:
                $img = 'biker.png';
                break;
            case 3:
                $img = 'tenis.png';
                break;
            case 4:
                $img = 'nordic.png';
                break;
            case 5:
                $img = 'football.png';
                break;
            case 6:
                $img = 'basketball.png';
                break;
            case 7:
                $img = 'gym.png';
                break;
            case 8:
                $img = 'volleyball.png';
                break;
            case 9:
                $img = 'bilard.png';
                break;
            case 10:
                $img = 'swimming.png';
                break;
            case 11:
                $img = 'run.png';
                break;
            default:
                $img = 'run.png';
                break;
        }

        return $img;
    }
}
