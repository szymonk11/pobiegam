<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TemplateManager
{
    protected static $_javascripts = array();
    protected static $_csses = array();

    public static function add_javascript($urls) {
        if(is_array($urls)) {
            self::$_javascripts = array_unique(array_merge(self::$_javascripts, $urls));
        } else {
            if(!in_array($urls, self::$_javascripts))
                array_push(self::$_javascripts, $urls);
        }
    }

    public static function render_javascripts() {
        foreach(self::$_javascripts as $js) {
            echo '<script type="text/javascript" src="'.base_url($js).'"></script>'.PHP_EOL;
        }
    }

    public static function add_css($urls) {
        if(is_array($urls)) {
            self::$_csses = array_unique(array_merge(self::$_csses, $urls));
        } else {
            if(!in_array($urls, self::$_csses))
                array_push(self::$_csses, $urls);
        }
    }

    public static function render_css() {
        foreach(self::$_csses as $css) {
            echo '<link rel="stylesheet" href="'.base_url($css).'" />'.PHP_EOL;
        }
    }
}