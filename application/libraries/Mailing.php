<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require APPPATH.'third_party/PHPMailer/Exception.php';
require APPPATH.'third_party/PHPMailer/PHPMailer.php';
require APPPATH.'third_party/PHPMailer/SMTP.php';

class Mailing
{
    protected $CI;
    protected $mail;

    protected $CONFIG = array();
    protected $language = 'pl';
    protected $email = 'kontakt@pobiegam.net';
    protected $email_name = 'Pobiegam.NET';
    
    public function __construct() {
        $this->CI = & get_instance();

        $this->email = $this->CI->config->item('email_email');
        $this->email_name = $this->CI->config->item('email_name');
        $this->CONFIG['host'] = $this->CI->config->item('smtp_host');
        $this->CONFIG['username'] = $this->CI->config->item('smtp_username');
        $this->CONFIG['password'] = $this->CI->config->item('smtp_password');

        $this->mail = new PHPMailer(); 
        $this->init();
    }

    protected function init()
    {
        $this->mail->CharSet = 'UTF-8';
        $this->mail->Encoding = 'base64';
        $this->mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $this->mail->isSMTP();                                      // Set mailer to use SMTP
        $this->mail->Host = $this->CONFIG['host'];  				// Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = $this->CONFIG['username'];          // SMTP username
        $this->mail->Password = $this->CONFIG['password'];          // SMTP password
        $this->mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = 465;
        $this->mail->setLanguage($this->language, APPPATH.'third_party/PHPMailer/language/');
    }

    public function send_contact_msg($email, $text, $name)
    {
        $this->mail->setFrom($email, $name);
        $this->mail->addAddress($this->email, $this->email_name);
        $this->mail->addReplyTo($email, $name);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Formularz kontaktowy Pobiegam.NET - '.$name;
        $this->mail->Body = $this->CI->load->view('emails/contact', array(
            'name' => $name,
            'email' => $email,
            'txt' => nl2br($text),
            'prev_url' => base_url('emails/contact?name='.$name.'&email='.$email.'&txt='.$text)
        ), true);
        $this->mail->AltBody = $text;
        
        return $this->mail->send();
    }
    
    public function send_add_post_msg($email, $name, $unikey, $expire)
    {
        $this->mail->setFrom($this->email, $this->email_name);
        $this->mail->addAddress($email, $name);

        $email = urlencode($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Twoje Ogłoszenie w Pobiegam.NET - WAŻNE INFORMACJE';
        $this->mail->Body = $this->CI->load->view('emails/add_post', array(
            'name' => $name,
            'expire' => $expire,
            'unikey' => $unikey,
            'to' => $email,
            'prev_url' => base_url('emails/add_post?name='.$name.'&expire='.$expire.'&unikey='.$unikey.'&to='.$email)
        ), true);
        
        $text = 'Witaj, '.(isset($name)?$name:'').'! Dziękujemy Ci serdecznie za zamieszczenie ogłoszenia w naszym serwisie! Mamy nadzieję, że dzięki nam znajdziesz kogoś do wspólnej aktywności sportowej na świeżym powietrzu. Poniżej przesyłamy Ci kilka istotnych informacji dotyczących Twojego ogłoszenia. Pozdrawiamy, Ekipa Pobiegam.NET!'
            .' Ważne informacje: Data wygaśnięcia ogłoszenia: '.(isset($expire)?$expire:'').' Unikatowy adres do natychmiastowego usunięcia ogłoszenia: '.(base_url('posts/delete?unikey='.$unikey.'&email='.$email));
        $this->mail->AltBody = $text;
        
        return $this->mail->send();
    }

    public function send_add_post_accept_msg($email, $name, $unikey, $expire)
    {
        $this->mail->setFrom($this->email, $this->email_name);
        $this->mail->addAddress($email, $name);

        $email = urlencode($email);

        $this->mail->isHTML(true);
        $this->mail->Subject = 'Twoje Ogłoszenie w Pobiegam.NET - WAŻNE INFORMACJE';
        $this->mail->Body = $this->CI->load->view('emails/add_post_accept', array(
            'name' => $name,
            'expire' => $expire,
            'unikey' => $unikey,
            'to' => $email,
            'prev_url' => base_url('emails/add_post_accept?name='.$name.'&expire='.$expire.'&unikey='.$unikey.'&to='.$email)
        ), true);
        
        $text = 'Witaj, '.(isset($name)?$name:'').'! Dziękujemy Ci serdecznie za zamieszczenie ogłoszenia w naszym serwisie! Mamy nadzieję, że dzięki nam znajdziesz kogoś do wspólnej aktywności sportowej na świeżym powietrzu. Poniżej przesyłamy Ci kilka istotnych informacji dotyczących Twojego ogłoszenia. Pozdrawiamy, Ekipa Pobiegam.NET!'
            .' Kliknij w ten link, aby potwierdzić umieszczenie ogłoszenia: '.base_url('posts/accept?unikey='.$unikey.'&email='.$to)
            .' Ważne informacje: Data wygaśnięcia ogłoszenia: '.(isset($expire)?$expire:'').' Unikatowy adres do natychmiastowego usunięcia ogłoszenia: '.(base_url('posts/delete?unikey='.$unikey.'&email='.$email));
        $this->mail->AltBody = $text;
        
        return $this->mail->send();
    }
    
}
