<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts_model extends CI_Model {

    private $table = 'posts';

    public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }
    
    public function getPostDetails($post_id)
    {
        return $this->db->where('id', $post_id, TRUE)
            ->get($this->table, 1)
            ->row();
    }

    public function getPostByUnikeyEmail($unikey, $email)
    {
        return $this->db->where('unikey', $unikey)
            ->where('email', $email)
            ->get($this->table, 1)
            ->row();
    }

    public function getLastPosts($num = 10, $visible = POST_VISIBLE)
    {
		$this->db->select('id,add_date,type,level,city,date,time,user,name,age,desc,email,telephone,gg');
		
        $this->db->order_by('add_date DESC, id DESC');

        if($visible)
            $this->db->where('visible', POST_VISIBLE);
        
        return $this->db->get($this->table, $num)->result();
    }

    public function getPosts($page = 1, $per_page = NULL)
    {
        if(!$per_page)
            $per_page = $this->config->item('per_page');

        $offset = $page * $per_page - $per_page;
        
        return $this->db->select('id,add_date,type,level,city,date,time,user,name,age,desc,email,telephone,gg')
			->where('visible', POST_VISIBLE)
            ->order_by('add_date DESC, id DESC')
            ->limit($per_page, $offset)
            ->get($this->table)
            ->result();
    }

    public function getMaxPages($per_page = NULL, $visible = POST_VISIBLE)
    {
        if(!$per_page)
            $per_page = $this->config->item('per_page');

        if($per_page < 1)
            $per_page = 1;

        if($visible)
            $this->db->where('visible', POST_VISIBLE);

        $count = $this->db->count_all_results($this->table);
        return ceil($count/$per_page);
    }

    public function getSearchPosts($page = 1, $type, $city, $per_page = NULL)
    {
        if(!$per_page)
            $per_page = $this->config->item('per_page');

        $offset = $page * $per_page - $per_page;

		$this->db->select('id,add_date,type,level,city,date,time,user,name,age,desc,email,telephone,gg');
		
        $this->db->where('visible', POST_VISIBLE);
	    if($type != '0' && $type != '')
            $this->db->where('type', $type);
            
	    if($city != '0' && $city != '')
	        $this->db->like('city', $city);
	
	    return $this->db->order_by('add_date DESC, id DESC')
		    ->limit($per_page, $offset)
            ->get($this->table)
            ->result();
    }

    public function getSearchMaxPages($type, $city, $per_page = NULL)
    {
        if(!$per_page)
            $per_page = $this->config->item('per_page');

        if($per_page < 1)
            $per_page = 1;

        $this->db->where('visible', POST_VISIBLE);
        if($type != '0' && $type != '')
            $this->db->where('type', $type);
            
	    if($city != '0' && $city != '')
	        $this->db->like('city', $city);

        $count = $this->db->count_all_results($this->table);
        return ceil($count/$per_page);
    }

    public function addPost($data)
    {
        return $this->db->insert($this->table, array(
            'visible' => (isset($data['visible'])) ? $data['visible'] : POST_VISIBLE,
            'add_date' => time(),
            'type' => $data['type'],
            'level' => $data['level'],
            'city' => $data['city'],
            'date' => $data['date'],
            'time' => $data['time'],
            'user' => 0,
            'name' => $data['name'],
            'age' => $data['age'],
            'desc' => $data['description'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'expire' => $data['expire'],
            'unikey' => $data['unikey'],
            'marketing' => isset($data['marketing']) ? $data['marketing'] : 0
        ));
    }

    public function delete($post_id, $permament = FALSE)
    {
        if($permament)
            return $this->db->where('id', $post_id)->delete($this->table);

        return $this->db->where('id', $post_id)->update($this->table, array('visible' => POST_INVISIBLE));
    }

    public function accept($post_id)
    {
        return $this->db->where('id', $post_id)->update($this->table, array('visible' => POST_VISIBLE));
    }
    
    public function getPostsCount($active = TRUE)
    {
        if($active) {
            $this->db->where('visible', POST_VISIBLE);
        }
        
        return $this->db->count_all_results($this->table);
    }

    public function getAllPosts($active = TRUE)
    {
        if($active) {
            $this->db->where('visible', POST_VISIBLE);
        }
        
        return $this->db->get($this->table)->result();
    }


    public function getValidationRules()
    {
        return array(
            array(
                'field' => 'type',
                'label' => 'typ sportu',
                'rules' => 'required|greater_than_equal_to[0]',
                'errors' => array('required' => 'Wybierz typ uprawianego sportu.', 'greater_than_equal_to' => 'Wybierz typ uprawianego sportu.')
            ),
            array(
                'field' => 'level',
                'label' => 'poziom',
                'rules' => 'required|greater_than[0]|less_than[11]',
                'errors' => array('required' => 'Wybierz swój poziom.', 'greater_than' => 'Podaj poprawny poziom.', 'less_than' => 'Podaj poprawny poziom.')
            ),
            array(
                'field' => 'city',
                'label' => 'miejsce',
                'rules' => 'required',
                'errors' => array('required' => 'Podaj swoją miejscowość.')
            ),
            array(
                'field' => 'time_from',
                'label' => 'czas od',
                'rules' => 'required|regex_match[/^[0-2][0-9]:[0-5][0-9]$/]',
                'errors' => array('required' => 'Podaj, od której godziny ćwiczysz.', 'regex_match' => 'Czas musi zostać podany w formacie HH:MM.')
            ),
            array(
                'field' => 'time_to',
                'label' => 'czas do',
                'rules' => 'required|regex_match[/^[0-2][0-9]:[0-5][0-9]$/]',
                'errors' => array('required' => 'Podaj, do której godziny ćwiczysz.', 'regex_match' => 'Czas musi zostać podany w formacie HH:MM.')
            ),
            array(
                'field' => 'name',
                'label' => 'imię',
                'rules' => 'required',
                'errors' => array('required' => 'Podaj swoje imię.')
            ),
            array(
                'field' => 'age',
                'label' => 'wiek',
                'rules' => 'required|greater_than[13]|less_than[199]',
                'errors' => array('required' => 'Podaj swój wiek.', 'greater_than' => 'Niestety, jesteś zbyt młody.', 'less_than' => 'To możliwe?')
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email',
                'errors' => array('required' => 'Podaj poprawny adres e-mail.', 'valid_email' => 'Podaj poprawny adres e-mail.')
            ),
            array(
                'field' => 'reg',
                'label' => '',
                'rules' => 'required|greater_than[0]',
                'errors' => array('required' => 'Musisz potwierdzić akceptację regulaminu.', 'greater_than' => 'Musisz potwierdzić akceptację regulaminu.')
            )
        );
    }

}
