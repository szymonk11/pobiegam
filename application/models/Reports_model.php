<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model
{
    private $table = 'reports';

    public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }

    public function getAllReports()
    {
        return $this->db->get($this->table)->result();
    }

    public function addReport($data)
    {
        return $this->db->insert($this->table, array(
            'post_id' => $data['post_id'],
            'status' => $data['status'],
            'ip' => $data['ip'],
            'desc' => $data['desc'],
            'added' => time()
        ));
    }

    public function getReportsCount($new = FALSE)
    {
        if($new) {
            //$this->db->where('status', 1);
        }
        
        return $this->db->count_all_results($this->table);
    }

    public function changeStatus($report_id, $status)
    {
        return $this->db->update($this->table, array('status' => $status), 'id = '.$report_id);
    }
   
}
