<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
    // Nazwa tabeli, z której będziemy korzystać w modelu
    public $table = 'users';

    public function __construct()
	{
        parent::__construct();
        $this->load->database();
    }

    /**
     * Logowanie użytkownika 
     * Sprawdza, czy użytkownik o podanym adresie email i haśle istnieje w bazie danych
     *
     * @access	public
     * @return	mixed
    */
    public function login($email, $password)
    {
        $user = $this->db->select('password, salt, name, admin, rank, registered_date')->where('email', $email)->get($this->table)->row_array();
        if($user) {

            //$pass = substr(md5(md5($user['salt']).md5($password)), 0, 256);  

            if(password_verify($password, $user['password'])) {

                unset($user['salt']);
                unset($user['password']);

                return $user;
            }
        }
        return false;
    }

    public function get_count_users()
    {
        return $this->db->count_all_results($this->table);
    }
}
