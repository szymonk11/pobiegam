<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ogłoszenia
        <small>Edytuj ogłoszenie</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('pa/ogloszenia'); ?>"><i class="fa fa-tag"></i> Ogłoszenia</a></li>
        <li class="active">Edytuj ogłoszenie</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
	
	<?php if($error): ?>
	
	<div class="alert alert-warning alert-dismissible">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <h4><i class="icon fa fa-warning"></i> Wystąpił błąd!</h4>
	    <?= $error; ?>
	</div>
	
	<?php endif; ?>
	
	<div class="box box-primary">
	    <div class="box-header with-border">
		<h3 class="box-title">Edycja ogłoszenia</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form role="form" method="POST">
		<div class="box-body">
		    <div class="form-group">
			<label for="isport">Typ</label>
			<select name="sport" id="isport" class="form-control">
			    <?php foreach($sports as $key => $value): ?>
				<option value="<?= $key ?>" <?= ($post->type == $key) ? 'selected' : '' ?>><?= $value['name']; ?></option>
			    <?php endforeach; ?>
			</select>
		    </div>
		    <div class="form-group">
			<label for="post-level">Poziom</label>
			<select name="level" id="post-level" class="form-control">
			    <option value="1" <?= ($post->level == 1) ? 'selected' : '' ?>>Amator</option>
			    <option value="2" <?= ($post->level == 2) ? 'selected' : '' ?>>Średniozaawansowany</option>
			    <option value="3" <?= ($post->level == 3) ? 'selected' : '' ?>>Zaawansowany</option>
			    <option value="4" <?= ($post->level == 4) ? 'selected' : '' ?>>Ekspert</option>
			</select>
		    </div>
		    <div class="form-group">
			<label for="icity">Miejscowość</label>
			<input type="text" name="city" id="icity" class="form-control" placeholder="Wpisz miejscowość" value="<?= $post->city; ?>" required="" />
		    </div>
		    
		    <div class="form-group">
			<label for="idate">Kiedy</label>
			<input type="text" name="date" id="idate" class="form-control" value="<?= $post->date; ?>" />
			
			<label for="itimefrom">W godzinach</label><br />
			<?php
			    $time = explode(' - ', $post->time);
			?>
			<input type="time" name="timefrom" id="itimefrom" placeholder="hh:mm" value="<?= $time[0] ?>" required="" /> - <input type="time" name="timeto" id="itimeto" placeholder="hh:mm" value="<?= $time[1] ?>" required="" />
		    </div>
		    
		    <div class="form-group">
			<div class="row">
			    <div class="col-xs-6 col-lg-4">
				<label for="iname">Imię</label>
				<input type="text" id="iname" name="name" maxlength="32" class="form-control" placeholder="" value="<?= $post->name; ?>" required="" />
			    </div>
			    <div class="col-xs-6 col-lg-4">
				<label for="iage">Wiek</label>
				<input type="number" id="iage" name="age" min="13" max="120" maxlength="3" class="form-control" placeholder="" value="<?= $post->age; ?>" required="" />
			    </div>
			</div>
		    </div>
		    
		    <div class="form-group">
			<div class="row">
			    <div class="col-xs-12 col-lg-4">
				<label for="imail">E-mail</label>
				<input type="email" name="email" id="imail" maxlength="128" class="form-control" placeholder="" value="<?= $post->email; ?>" required="" />
				<p class="help-block">Niezbędny do zarządzania ogłoszeniem.</p>
                                Wyślij mail z informacjami: <span id="send-info-mail" class="btn btn-primary"><i class="fa fa-envelope"></i></span>
			    </div>
			    <div class="col-xs-6 col-lg-4">
				<label for="itel">Telefon</label>
				<input type="tel" name="tel" id="itel" maxlength="32" class="form-control" value="<?= $post->telephone; ?>" placeholder="" />
			    </div>
			    <div class="col-xs-6 col-lg-4">
				<label for="igg">GG</label>
				<input type="text" name="gg" id="igg" maxlength="32" class="form-control" value="<?= $post->gg; ?>" placeholder="" />
			    </div>
			</div>
		    </div>
		    
		    <div class="form-group">
			<label for="idesc">Dodatkowe informacje</label>
			<textarea name="desc" id="idesc" rows="8" maxlength="512" class="form-control"><?= $post->desc; ?></textarea>
		    </div>
		    
		    <div class="form-group">
			<label for="iexpire">Data wygaśnięcia</label>
			<input type="text" name="expire" id="iexpire" class="form-control" placeholder="" value="<?= date('d-m-Y H:i', $post->expire) ?>" required="" />
			<p class="help-block">UWAGA! Wymagany format: DD-MM-YYYY HH:II ! W przeciwnym razie wystąpi błąd.</p>
		    </div>
		    
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
		    <a href="<?= base_url('pa/ogloszenia'); ?>"><span class="btn btn-default">Wróc</span></a>
		    <button type="submit" class="btn btn-info pull-right">Zapisz zmiany</button>
		</div>
	    </form>
	</div>

    </section>
</div>