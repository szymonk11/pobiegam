<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Dashboard

      <small>Panel kontrolny</small>

    </h1>

    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>

      <li class="active">Panel</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content container-fluid">



      <div class="col-lg-4 col-xs-6">

          <div class="small-box bg-green">

              <div class="inner">

                  <h3><?= $posts_count ?></h3>

                  <p>Aktywnych ogłoszeń</p>

              </div>

              <div class="icon">

                  <i class="ion ion-pin"></i>

              </div>

              <a href="<?= base_url('pa/ogloszenia'); ?>" class="small-box-footer">Zobacz ogłoszenia <i class="fa fa-arrow-circle-right"></i></a>

          </div>

      </div>



      <div class="col-lg-4 col-xs-6">

          <div class="small-box bg-red">

              <div class="inner">

                  <h3><?= $reports_count ?></h3>

                  <p>Raportów</p>

              </div>

              <div class="icon">

                  <i class="ion ion-alert"></i>

              </div>

              <a href="<?= base_url('pa/raporty'); ?>" class="small-box-footer">Przejrzyj <i class="fa fa-arrow-circle-right"></i></a>

          </div>

      </div>



      <div class="col-lg-4 col-xs-6">

          <div class="small-box bg-yellow">

              <div class="inner">

                  <h3><?= $users_count ?></h3>

                  <p>Użytkowników</p>

              </div>

              <div class="icon">

                  <i class="ion ion-person"></i>

              </div>

              <a href="#" class="small-box-footer">Zobacz liste <i class="fa fa-arrow-circle-right"></i></a>

          </div>

      </div>



  </section>

</div>

<!-- /.content-wrapper -->