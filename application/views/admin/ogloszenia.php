  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        Ogłoszenia

        <small>Lista ogłoszeń</small>

      </h1>

      <ol class="breadcrumb">

        <li><a href="/pa/ogloszenia"><i class="fa fa-tag"></i> Ogłoszenia</a></li>

        <li class="active">Lista ogłoszeń</li>

      </ol>

    </section>



    <!-- Main content -->

    <section class="content container-fluid">

        

	<?php if(isset($error)): ?>

	

	<div class="alert alert-warning alert-dismissible">

	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

	    <h4><i class="icon fa fa-warning"></i> Wystąpił błąd!</h4>

	    <?= $error; ?>

	</div>

	

	<?php endif; ?>

	

	<?php if(isset($success)): ?>

	

	<div class="alert alert-success alert-dismissible">

	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

	    <h4><i class="icon fa fa-warning"></i> Sukces!</h4>

	    <?= $success; ?>

	</div>

	

	<?php endif; ?>

	

        <div class="box">

            <div class="box-header">

              <h3 class="box-title">Lista wszystkich ogłoszeń</h3>

            </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="example1" width="100%" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">

                <thead>

                <tr role="row">

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Typ: activate to sort column ascending">ID</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Typ: activate to sort column ascending">Typ</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Miejscowosc: activate to sort column ascending">Miejscowość</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Data: activate to sort column ascending">Data dodania</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Uzytkownik: activate to sort column ascending">Użytkownik</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Poziom: activate to sort column ascending">Poziom</th>

                    <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 134.000px;" aria-label="">Akcja</th>

                </tr>

                </thead>

                <tbody>

                <?php foreach($posts as $key => $post): ?>

                    

                    <?php $class = 'odd'; if($key%2 == 0) {$class = 'even';} ?>

                    

                    <tr role="row" class="<?= $class ?>">

                        <td><?= $post->id ?> <?= ($post->visible == POST_INVISIBLE) ? '<i class="fa fa-close"></i>':''; ?></td>

                        <td class=""><?= get_name_by_type($post->type) ?></td>

                        <td class=""><?= $post->city ?></td>

                        <td class="sorting_1"><?= date('d/m/y, H:i', $post->add_date) ?></td>

                        <td><?= $post->name ?> <small>(<?= $post->email ?>)</small></td>

                        <td><?= $post->level; ?></td>

                        <td class="text-center">
                          <?= ($post->visible == POST_INVISIBLE) ? '<a href="'.base_url('pa/ogloszenia/accept/'.$post->id).'"><button type="button" class="btn btn-info" style="margin-right: 10px;"><i class="fa fa-check"></i></button></a>':''; ?>
                          <a href="<?= base_url('pa/ogloszenia/edytuj/'.$post->id); ?>"><button type="button" class="btn btn-warning" style="margin-right: 10px;"><i class="fa fa-edit"></i></button></a><a href="<?= base_url('pa/ogloszenia/usun/'.$post->id); ?>"><button type="button" class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash"></i></button></a>
                        </td>

                    </tr>

                    

                <?php endforeach; ?>

                </tbody>

                <tfoot>

                <tr>

                    <th rowspan="1" colspan="1">ID</th>

                    <th rowspan="1" colspan="1">Typ</th>

                    <th rowspan="1" colspan="1">Miejscowość</th>

                    <th rowspan="1" colspan="1">Data dodania</th>

                    <th rowspan="1" colspan="1">Użytkownik</th>

                    <th rowspan="1" colspan="1">Poziom</th>

                    <th rowspan="1" colspan="1">Akcja</th>

                </tr>

                </tfoot>

              </table>

            </div>

            <!-- /.box-body -->

          </div>



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->