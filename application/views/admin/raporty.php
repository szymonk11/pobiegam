  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        Zgłoszenia

        <small>Lista zgłoszeń</small>

      </h1>

      <ol class="breadcrumb">

        <li><a href="<?= base_url('pa/ogloszenia'); ?>"><i class="fa fa-tag"></i> Raporty</a></li>

        <li class="active">Lista raportów</li>

      </ol>

    </section>



    <!-- Main content -->

    <section class="content container-fluid">

        

	<?php if(isset($error)): ?>

	

	<div class="alert alert-warning alert-dismissible">

	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

	    <h4><i class="icon fa fa-warning"></i> Wystąpił błąd!</h4>

	    <?= $error; ?>

	</div>

	

	<?php endif; ?>

	

	<?php if(isset($success)): ?>

	

	<div class="alert alert-success alert-dismissible">

	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

	    <h4><i class="icon fa fa-warning"></i> Sukces!</h4>

	    <?= $success; ?>

	</div>

	

	<?php endif; ?>

	

        <div class="box">

            <div class="box-header">

              <h3 class="box-title">Lista wszystkich zgłoszeń</h3>

            </div>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="example1" width="100%" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">

                <thead>

                <tr role="row">

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Typ: activate to sort column ascending">ID</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Typ: activate to sort column ascending">Ogłoszenie</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Miejscowosc: activate to sort column ascending">Status</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Data: activate to sort column ascending">Data dodania</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Uzytkownik: activate to sort column ascending">IP</th>

                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Poziom: activate to sort column ascending">Treść</th>

                    <th tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 137.000px;" aria-label="">Akcja</th>

                </tr>

                </thead>

                <tbody>

                    

                <?php 

                    $status = array(

                        1 => array('name' => 'Nowe', 'class' => 'warning'),

                        2 => array('name' => 'W trakcie', 'class' => 'primary'),

                        3 => array('name' => 'Zakończone', 'class' => 'success'),

                        4 => array('name' => 'Anulowane', 'class' => 'danger')

                    );

                ?>

                    

                <?php foreach($reports as $key => $report): ?>

                    

                    <?php $class = 'odd'; if($key%2 == 0) {$class = 'even';} ?>

                    

                    <tr role="row" class="<?= $class ?>">

                        <td><?= $report->id ?></td>

                        <td class=""><a href="<?= base_url('pa/ogloszenia/edytuj/').$report->post_id; ?>"><i class="fa fa-tag"></i> <?= $report->post_id ?></a></td>

                        <td class=""><span class="label label-<?= $status[$report->status]['class'] ?>"><?= $status[$report->status]['name'] ?></span></td>

                        <td class="sorting_1"><?= date('d/m/y, H:i', $report->added) ?></td>

                        <td><?= $report->ip; ?></td>

                        <td><?= substr($report->desc, 0, 64) ?></td>

                        <td class="text-center">

                            <?php if($report->status == 2) :?>

                            <a href="<?= base_url('pa/raporty/').$report->id; ?>?action=nowy"><button type="button" class="btn btn-warning" style="margin-right: 5px; font-size: 8px; padding: 6px 10px;"><i class="fa fa-pause"></i></button></a>

                            <?php else: ?>

                            <a href="<?= base_url('pa/raporty/').$report->id; ?>?action=start"><button type="button" class="btn btn-primary" style="margin-right: 5px; font-size: 8px; padding: 6px 10px;"><i class="fa fa-play"></i></button></a>

                            <?php endif; ?>

                            <a href="<?= base_url('pa/raporty/').$report->id; ?>?action=zakoncz"><button type="button" class="btn btn-success" style="margin-right: 5px; font-size: 8px; padding: 6px 10px;"><i class="fa fa-check"></i></button></a>

                            <a href="<?= base_url('pa/raporty/').$report->id; ?>?action=anuluj"><button type="button" class="btn btn-danger" style="margin-right: 5px; font-size: 8px; padding: 6px 10px;"><i class="fa fa-close"></i></button></a>

                        </td>

                    </tr>

                    

                <?php endforeach; ?>

                </tbody>

                <tfoot>

                <tr>

                    <th rowspan="1" colspan="1">ID</th>

                    <th rowspan="1" colspan="1">Ogłoszenie</th>

                    <th rowspan="1" colspan="1">Status</th>

                    <th rowspan="1" colspan="1">Data dodania</th>

                    <th rowspan="1" colspan="1">IP</th>

                    <th rowspan="1" colspan="1">Treść</th>

                    <th rowspan="1" colspan="1">Akcja</th>

                </tr>

                </tfoot>

              </table>

            </div>

            <!-- /.box-body -->

          </div>



    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->