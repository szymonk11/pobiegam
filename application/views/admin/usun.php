<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ogłoszenia
        <small>Usuń ogłoszenie</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/pap/ogloszenia"><i class="fa fa-tag"></i> Ogłoszenia</a></li>
        <li class="active">Usuń ogłoszenie</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
	
	<div class="callout callout-info col-xs-12 col-sm-6">
	    <h4>Czy na pewno chcesz usunąć to ogłoszenie?</h4>
	    
	    <p><?= sprintf('%s - %s - %s (%s) - %s - Dodane: %s', sport_name($post['type']), level_name($post['level']), $post['name'], $post['age'], $post['email'], date('d/m/y, H:i', $post['add_date'])) ?></p>

	    <a href="/pap/ogloszenia/usun/<?= $post['id'] ?>?confirm=true"><button type="button" class="btn btn-success">USUŃ</button></a>
	    <a href="/pap/ogloszenia"><button type="button" class="btn btn-danger pull-right">ANULUJ</button></a>
	</div>
	
    </section>
</div>


