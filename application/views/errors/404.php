<div class="main main-raised">
    <div id="content" class="container">
        <div class="section text-center">

            <h2 class="title">Przepraszamy! Nie znaleźlimy takiej strony.</h2>
            <div><a href="<?= base_url(); ?>">Wróc na stronę główną</a></div>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>
