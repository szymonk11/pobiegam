
<div class="main main-raised">
    <div class="container">
        <div class="section text-center">

            <?php $this->load->view('layout/alerts'); ?>

            <h2 class="title">Najnowsze ogłoszenia</h2>

            <div class="table-responsive">
                <table class="table-posts table table-hover">

                    <tbody>
                        <?php foreach($posts as $post): ?>
                        <tr>
                            <td><img src="<?= base_url('web/img/types/'.get_img_name_by_type($post->type)); ?>" style="height: 50px;" alt="<?= get_name_by_type($post->type); ?>" /></td>
                            <td><i class="material-icons">location_on</i> <?= $post->city; ?></td>
                            <td class="d-none d-sm-table-cell"><i class="material-icons">calendar_today</i> <?= $post->date; ?></td>
                            <td><i class="material-icons">access_time</i> <?= $post->time; ?></td>
                            <td><a href="<?= get_post_url($post); ?>"><button class="btn btn-sm btn-success btn-pobiegam">Więcej<div class="ripple-container"></div></button></a></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>

            <a href="<?= base_url('posts/'); ?>"><button class="btn btn-success btn-pobiegam">Zobacz wszystkie<div class="ripple-container"></div></button></a>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>