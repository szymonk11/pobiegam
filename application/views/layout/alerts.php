<?php
$message_success = (isset($message_success)) ? $message_success : ( isset($_SESSION['message_success']) ? $_SESSION['message_success'] : NULL );
$message_error = (isset($message_error)) ? $message_error : ( isset($_SESSION['message_error']) ? $_SESSION['message_error'] : NULL );

if($message_success): ?>
<div class="alert alert-success" role="alert">
    <?= $message_success; ?>
</div>
<?php
endif;

if($message_error): ?>
<div class="alert alert-danger" role="alert">
    <?= $message_error; ?>
</div>
<?php endif; ?>
