    <footer class="footer footer-default" >
        <div class="container">
            <nav class="float-left">
                <ul>
                    <li><a class="facebook facebook-footer" href="https://pl-pl.facebook.com/pobiegamnet" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="<?= base_url('onas#content'); ?>">O co chodzi?</a></li>
                    <li><a href="<?= base_url('regulamin#content'); ?>">Regulamin</a></li>
                    <li><a href="<?= base_url('kontakt#content'); ?>">Kontakt</a></li>
                    <li><a href="<?= base_url('kontakt#content'); ?>">Współpraca</a></li>
                    <li><a id="scroll_top" href="#top">Skocz do góry</a></li>
                </ul>
            </nav>
            <div class="copyright float-right">
                Copyright &copy; 2019 by <a href="<?= base_url() ?>"><strong>Pobiegam.net</strong></a>
            </div>
        </div>
    </footer>

    <script>
    function initAutocomplete()
    {
        var options = {
            types: ['(cities)'],
            componentRestrictions: {country: 'pl'}
        };

        var input = document.getElementById('icity');
        if(input) {
            var autocomplete = new google.maps.places.Autocomplete(input, options);
        }

        var input2 = document.getElementById('TopSearchCity');
        if(input2) {
            var autocomplete2 = new google.maps.places.Autocomplete(input2, options);
        }
    }
    </script>

    <script type="text/javascript" src="<?= base_url('web/js/core/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('web/js/core/popper.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('web/js/core/bootstrap-material-design.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('web/js/plugins/moment.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('web/js/material-kit.js?v=2.0.5'); ?>"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD62rn2cHBCnWjLyy0SeuUlwndZ6GVPDA&libraries=places&region=pl&callback=initAutocomplete" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script type="text/javascript" src="<?= base_url('web/js/notify.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= base_url('web/js/pobiegam.js'); ?>"></script>

    <?php TemplateManager::render_javascripts(); ?>

</body>
</html>