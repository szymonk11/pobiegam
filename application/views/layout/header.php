<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    
    <title><?= isset($title) ? $title : 'Pobiegam.NET - Portal łączący ludzi z pasją!'; ?></title>
    
    <meta name="description" content="Pobiegam.NET to w pełni darmowa inicjatywa, mająca na celu zachęcenie ludzi do uprawiania sportu i integracji społecznej. Platforma pozwala nie tylko na zdobywanie nowych znajomości, ale przede wszystkim motywuje do wyjścia z domu." />
    <meta name="keywords" content="pobiegam, pobiegam.net, szukam partnera, do biegania, bieg, na rower, sport, partner, fitness, tenis, nordic walking, piłka nożna, koszykowka, siatkówka, jazda, na, rowerze, spotted, szukam, poszukuję, bilard, siłownia, tenis, sport, znajomi, koledzy, współpraca, wyjście" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta property="og:image" content="<?= base_url('web/img/pobiegam_logo_250.png'); ?>" />
    <meta property="og:image:width" content="250" />
    <meta property="og:image:height" content="250" />
    <?php if(isset($post->desc)): ?>
    <meta property="og:description" content="<?= $post->desc; ?>" />
    <?php endif; ?>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('apple-touch-icon.png'); ?>" />
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('favicon-32x32.png'); ?>" />
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('favicon-16x16.png'); ?>" />
    <link rel="manifest" href="<?= base_url('site.webmanifest'); ?>" />
    <link rel="mask-icon" href="<?= base_url('safari-pinned-tab.svg'); ?>" color="#7dc246" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="theme-color" content="#ffffff" />
    <link rel="shortcut icon" href="<?= base_url('favicon.ico'); ?>" />

    <!-- Fonts and icons -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <?php TemplateManager::render_css(); ?>
    <link rel="stylesheet" href="<?= base_url('web/css/material-kit.css?v=2.0.5'); ?>" />
    <link rel="stylesheet" href="<?= base_url('web/css/main.css'); ?>" />
    
</head>
<body id="top">

<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top navbar-expand-lg"  color-on-scroll="100">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <div class="logo-image">
                    <img src="<?= base_url('web/img/logo.png'); ?>" alt="Pobiegam.NET - Szukasz znajomego do uprawiania sportu?" /> <span>Pobiegam.</span>NET
                </div>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="<?= base_url(''); ?>" class="nav-link">
                        <i class="material-icons">home</i> Strona główna
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('posts'); ?>" class="nav-link">
                        <i class="material-icons">search</i> Szukaj osoby
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url('add#content'); ?>" class="nav-link">
                        <i class="material-icons">create</i> Dodaj ogłoszenie
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="page-header header-filter" data-parallax="true" style="background-image: url('<?= base_url('web/img/girls.jpg'); ?>');background-position: center top;">
    <div class="container">
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
                <div class="brand text-center">
                    <h1 class="title">Znajdź osobę ze sportową pasją!</h1>
                    <div class="row">
                        <div id="header-searchbox" class="col-md-12">
                            <div class="form-group has-success mr-sm-2">
                                <select class="pobiegam-form-control form-control selectpicker" data-style="btn btn-link" size="" id="TopSearchType">
                                    <?php $active_type = (isset($search_data['type'])) ? $search_data['type'] : 0; ?>
                                    <?php foreach(get_all_sports() as $id => $sport): ?>
                                    <option value="<?= $id; ?>" <?= ($id == $active_type)?'selected':''; ?>><?= $sport['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        
                            <div class="form-group has-success mr-sm-2">
                                <i class="material-icons">location_on</i>
                                <input type="text" class="pobiegam-form-control form-control" id="TopSearchCity" placeholder="Wszędzie" value="<?= isset($search_data['city']) ? $search_data['city'] : ''; ?>" />
                            </div>
                       
                            <button id="TopSearchButton" class="btn btn-sm btn-success" data-url="<?= base_url('search'); ?>">Szukaj<div class="ripple-container"></div></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

