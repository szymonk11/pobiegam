
<div class="main main-raised">
    <div id="content" class="container">
        <div class="section">

            <h3>O Pobiegam.NET</h3>
            <p>
                Pobiegam.NET to darmowa inicjatywa, mająca na celu zachęcenie ludzi do uprawiania sportu i integracji społecznej. Platforma pozwala nie tylko na zdobywanie nowych znajomości, ale przede wszystkim motywuje do wyjścia z domu.
            </p>

            <h3>Jak to działa?</h3>
            <p>
                Nasz pomysł powstał ze względu na zapotrzebowanie współczesnego, żyjącego w ciągłym pośpiechu społeczeństwa dla którego ważne jest to, by wszystko odbywało się jak najprościej i najszybciej. Projekt ma na celu przede wszystkim ułatwić i usprawnić wyszukiwanie osób z podobną sportową pasją. Głównym założeniem naszej platformy jest zachęcenie do uprawiania sportu i integracji ludzi o podobnych pasjach. Działa to w bardzo prosty sposób, użytkownik portalu uzupełnia krótki formularz, w którym zaprasza do udziału w wybranej aktywności fizycznej (np. piłce nożnej, bieganiu, tenisie itp.) Osoby mogą wzajemnie odszukiwać i dodawać ogłoszenia interesującego ich typu ze swoich miejscowości, kontaktować się i wspólnie motywować do uprawiania sportu. Chcemy, aby nasza platforma działała globalnie, ale zrzeszała przede wszystkim lokalną społeczność. Dotychczas stworzyliśmy serwis internetowy, który jest idealnym początkiem, realizującym założenia naszej idei.
            </p>

            <h3>Co w przyszłości?</h3>
            <p>
                Głównym celem jest rozwój aplikacji mobilnych na platformy iOS i Windows. Pracujemy równocześnie nad zwiększeniem popularności oraz zasiegów.
            </p>

            <video width="100%" controls>
                <source src="<?= base_url('web/videos/pobiegam.mp4'); ?>" type="video/mp4">
            </video>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>
