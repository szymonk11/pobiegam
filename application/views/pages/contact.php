
<div class="main main-raised">
    <div id="content" class="container">
        <div class="section row">

            <form id="contactform" action="<?= base_url('contact_json') ?>" class="col-sm-6 pr-sm-5">
                <h4 class="title">Formularz kontaktowy</h4>

                <div class="has-success">
                    Imię:
                    <input id="iname" type="text" name="iname" class="pobiegam-form-control form-control" required />
                </div>
                <br />
                <div class="has-success">
                    E-mail:
                    <input id="imail" type="email" name="imail" class="pobiegam-form-control form-control" required />
                </div>
                <br />
                <div class="has-success">
                    Jak możemy pomóc?:
                    <textarea id="idesc" rows="8" maxlength="1024" name="description" class="pobiegam-form-control form-control" required></textarea>
                </div>
                <br />
                <div class="form-square">
                    Potwierdź swoją tożsamość<br /><br />
                    <div class="g-recaptcha" data-sitekey="6LcBsDIUAAAAAPSPA1xJQSSLca-lMbS8QL-fiwM8"></div>
                </div>

                <div class="text-center" style="margin-top: 50px;">
                    <button id="insert-button" class="btn btn-success btn-pobiegam">Wyślij<div class="ripple-container"></div></button>
                </div>
            </form>

            <div id="contactdesc" class="col-sm-6 pl-sm-5 text-center">
                <h4 class="title">Masz jakieś pytanie? Potrzebujesz pomocy? Chcesz nawiązać współpracę lub przedstawić jakiś pomysł?<br><br>Napisz do nas!<br><br></h4>
                <img src="<?= base_url('web/img/') ?>cont.jpg" class="img-fluid" alt="Kontakt z Pobiegam.NET • Napisz do nas! • Współpraca, pytania, problemy, propozycje" />
            </div>
            <div class="clear"></div>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>

<?php
    TemplateManager::add_javascript('web/js/views/pages.contact.js');
?>
