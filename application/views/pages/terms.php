
<div class="main main-raised">
    <div id="content" class="container">
        <div class="section">

            <a name="start"></a>
            <h2 class="text-center">Regulamin Pobiegam.NET</h2>

            <ol class="list-reg list-style-none">
                <li class="margin-li"><h3>§1. Podstawa prawna</h3>
                    <p>Niniejsza Polityka Prywatności jest zgodna z przepisami wynikającymi z art. 13 ust. 1 i ust. 2 rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z 27 kwietnia 2016 r.</p>
                    <ol>
                        <li>Niniejszy regulamin określa zasady korzystania przez użytkowników z serwisu internetowego Pobiegam.NET, będącego forum przeznaczonym do publikowania informacji o poszukiwaniu osoby do współuprawiania sportu.</li>
                        <li>Celem Serwisu jest ułatwienie Użytkownikom znalezienia osoby do współuprawiania sportu, poprzez umożliwienie im zamieszczania ogłoszeń w Serwisie, komunikowania się między sobą oraz dotarcia do szerokiego kręgu odbiorców.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§2. Definicje</h3>
                    <ol>
                        <li><strong>Administracja</strong> — grupa osób, posiadająca nadzwyczajne uprawnienia do zarządzania Serwisem, podległa Właścicielowi.</li>
                        <li><strong>Konto Użytkownika</strong> — przydzielona danemu Użytkownikowi, identyfikowana za pomocą adresu e-mail, część Serwisu, za pomocą której Użytkownik może dokonywać określonych działań w ramach Serwisu.</li>
                        <li><strong>Rejestracja</strong> — oznacza czynność faktyczną dokonaną w sposób określony w Regulaminie, wymaganą dla korzystania przez Użytkownika ze wszystkich funkcjonalności Serwisu.</li>
                        <li><strong>Hasło</strong> - oznacza ciąg znaków literowych, cyfrowych lub innych wybranych przez Użytkownika podczas Rejestracji w Serwisie, wykorzystywanych w celu zabezpieczenia dostępu do Konta Użytkownika w Serwisie.</li>
                        <li><strong>Ogłoszenie</strong> — sporządzone przez Użytkownika ogłoszenie dotyczące uprawianego sportu, będące zaproszeniem do wspólnej aktywności, zamieszczane w Serwisie, na warunkach przewidzianych w Regulaminie.</li>
                        <li><strong>Pobiegam.NET, Serwis lub Strona Internetowa</strong> — internetowy serwis ogłoszeniowy pod nazwą Pobiegam.NET, prowadzony przez Administrację w języku polskim, umożliwiający zamieszczanie i przeglądanie Ogłoszeń, dostępny w domenie internetowej Pobiegam.NET, a także w ramach innych serwisów internetowych prowadzonych przez Administrację.</li>
                        <li><strong>Użytkownik</strong> — każda osoba korzystająca z serwisu.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§3. Dane osobowe</h3>
                    <ol>
                        <li>Wyłączne prawa Administratora Danych Osobowych przysługują administracji Serwisu.</li>
                        <li>Przetwarzanie danych osobowych odbywa się na podstawie art. 6 RODO i w celach marketingowch Administrator Danych Osobowych powołuje się na prawnie uzasadniony interes, którym jest analiza oczekiwań i dostosowywanie oferty do potrzeb Użytkownika.</li>
                        <li>Dane osobowe będą przechowywane przez okres 5 lat do czasu wykorzystania możliwości marketingowych i analizy danych potrzebnych do prowadzenia Serwisu lub do odwołania zgody na przetwarzanie danych osobowych.
                            <ol type="a">
                                <li>Użytkownikowi przysługuje prawo do wycofania się z tej zgody w dowolnym terminie. Dane osobowe będą przetwarzane do czasu jej odwołania.</li>
                            </ol>
                        </li>
                        <li>Użytkownik ma prawo dostępu danych osobowych, ich sprostowania, usunięcia lub ograniczenia przetwarzania.</li>
                        <li>Pytania dotyczące przetwarzania danych osobowych są kierowane do Administracji Serwisu.</li>
                        <li>Zasady przechowywania i dostęp do informacji określa polityka prywatności.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§4. Postanowienia ogólne</h3>
                    <ol>
                        <li>Założenie Konta lub zamieszczenie Ogłoszenia w Serwisie Pobiegam.NET jest jednoznaczne z akceptacją Regulaminu.</li>
                        <li>Użytkownik jest zobowiązany do śledzenia zmian Regulaminu, które wchodzą w życie w terminie 7 dni od dnia opublikowania.</li>
                        <li>Administracja informuje Użytkowników o zmianach w Regulaminie przez wysłanie pojedynczego maila.</li>
                        <li>Administracja nie ponosi odpowiedzialności za ewentualne straty spowodowane awarią serwera lub włamaniem.</li>
                        <li>Administracja nie ponosi odpowiedzialności za niezgodne z prawem czyny osób korzystających z Serwisu. Użytkownik odpowiada sam za siebie, a Administracja stara się tylko zapobiec niewłaściwemu zachowaniu.</li>
                        <li>Administracja na żądanie uprawnionych organów będzie udostępniać zgromadzone dane o Użytkownikach.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§5. Zasady i warunki korzystania z Serwisu</h3>
                    <ol>
                        <li>Ogłoszenia dostępne są dla wszystkich użytkowników Internetu. Wraz z Ogłoszeniem na stronie Serwisu zamieszczany jest formularz kontaktowy, który umożliwia każdemu użytkownikowi Internetu podgląd wszystkich podanych przez Użytkownika danych kontaktowych.</li>
                        <li>Serwis nie pobiera żadnych opłat za korzystanie z Serwisu.</li>
                        <li>Zamieszczanie ogłoszeń w Serwisie jest bezpłatne.</li>
                        <li>Użytkownicy ponoszą pełną odpowiedzialność za zamieszczane przez siebie treści.</li>
                        <li>Korzystając z Serwisu użytkownicy zobowiązani są przestrzegać ogólnie przyjętych zasad kultury:
                            <ol type="a">
                                <li>Obowiązuje posługiwanie się językiem polskim;</li>
                                <li>Obowiązuje całkowity zakaz używania wulgaryzmów;</li>
                                <li>Użytkownicy są zobowiązani do okazywania wzajemnego szacunku;</li>
                                <li>Wulgarność w stosunku do Administracji jak i Użytkowników będzie jednakowo karana;</li>
                                <li>Złośliwe opinie związane z projektem pozbawione odpowiedniej argumentacji będą usuwane.</li>
                            </ol>
                        </li>
                        <li>Treści zabronione:
                            <ol type="a">
                                <li>Treści powszechnie uznane za obraźliwe lub wprowadzające w błąd;</li>
                                <li>Ogłoszenia o charakterze towarzyskim;</li>
                                <li>Reklamy produktów, stron internetowych, serwisów, serwerów;</li>
                                <li>Odnośniki, linki przekierowujące użytkowników na strony o tematyce pornograficznej;</li>
                                <li>Treści niezgodne z obowiązującym prawem.</li>
                            </ol>
                        </li>
                        <li>Wszelkie rozwiązania techniczne, elementy graficzne i inne elementy strony WWW Serwisu, w szczególności kod HTML i XHTML, arkusze CSS, skrypty JavaScript podlegają ochronie prawnej pod kątem praw autorskich.</li>
                    </ol>
                </li>
     
                <li class="margin-li"><h3>§6. Konta</h3>
                    <ol>
                        <li>Użytkownik może aktywnie korzystać tylko z jednego Konta, w przeciwnym razie dostęp do Serwisu może zostać zablokowany.</li>
                        <li>Dostęp do Konta jest chroniony hasłem.</li>
                        <li>Konto łamiące postanowienia Regulaminu może zostać zablokowane lub trwale usunięte.</li>
                        <li>Nazwa Konta, awatar oraz pozostałe elementy profilu nie mogą zawierać treści prawnie zabronionych.</li>
                        <li>Rejestracja jest bezpłatna.</li>
                        <li>Zabronione jest rejestrowanie kont na dane osób postronnych oraz dane fikcyjne.</li>
                        <li>Konto w serwisie jest aktywowane poprzez potwierdzenie dostępu do konta e-mail lub ręczną aktywację przez Administrację.</li>
                        <li>Serwis przechowuje dane podane przy rejestracji i wykorzystuje je wyłącznie do wysyłania informacji o wzajemnej komunikacji między użytkownikami, komunikatów systemowych oraz w celach marketingowych.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§7. Ogłoszenia</h3>
                    <ol>
                        <li>Prezentowane w Serwisie Ogłoszenia i ich treść nie stanowią oferty w rozumieniu art. 66 Kodeksu Cywilnego.</li>
                        <li>Dodanie ogłoszenia realizowane jest przez wypełnienie przez użytkownika formularza dodawania ogłoszenia.</li>
                        <li>Użytkownik może zamieścić maksymalnie 5 ogłoszeń. Ogłoszenia nie mogą być duplikowane w celu zwiększenia zainteresowania.</li>
                        <li>Zamieszczanie ogłoszeń możliwe jest tylko we właściwych kategoriach.</li>
                        <li>Użytkownik zobowiązuje się, że prawidłowo oznaczy swoje Ogłoszenie.</li>
                        <li>Użytkownik zobowiązuje się do rzetelnego opisu ogłoszenia zgodnie z prawdą.</li>
                        <li>Ogłoszenia sprzeczne z regulaminem są usuwane.</li>
                        <li>Użytkownik zobowiązuje się do niezwłocznego usunięcia Ogłoszenia z serwisu po jego dezaktualizacji.</li>
                        <li>Administracja zastrzega sobie prawo do usunięcia Ogłoszeń dla których nie stwierdzono aktywności ogłoszeniodawcy przez 30 dni kalendarzowych.</li>
                        <li>Administracja może bez zgody użytkownika i bez uprzedniego powiadomienia użytkownika usunąć ogłoszenia i inne treści umieszczane przez użytkownika w serwisie.</li>
                        <li>Serwis zastrzega sobie prawo do wykorzystywania aktywnych Ogłoszeń i ich treści dla celów marketingowych i promocyjnych związanych z funkcjonowaniem serwisu.</li>
                        <li>Serwis zastrzega sobie prawo do utrzymywania w bazie danych Ogłoszeń, których okres emisji się zakończyła, w celu ich archiwizacji.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§8. Prawa i obowiązki Administracji</h3>
                    <ol>
                        <li>Administracja usuwa wszelkie niezgodne z Regulaminem lub prawem treści.</li>
                        <li>Administracja zastrzega sobie prawo zmiany niniejszego Regulaminu.</li>
                        <li>Administrator nie musi informować Użytkownika o powodzeniu usunięcia jego Ogłoszenia.</li>
                        <li>Administracja zastrzega możliwość czasowego wstrzymania pracy Serwisu ze względów technicznych lub przyczyn niezależnych od siebie.</li>
                    </ol>
                </li>

            </ol>
            <br /><br />
            <h2 class="text-center">Polityka prywatności</h2>

            <p>Administratorem odpowiedzialnym za zarządzanie danymi osobowymi w rozumieniu ogólnego rozporządzenia o ochronie danych (zwanego dalej „RODO”) i innych przepisów w zakresie ochrony danych osobowych obowiązujących w krajach członkowskich Unii Europejskiej oraz pozostałych regulacji w zakresie ochrony danych osobowych jest Administracja.</p>

            <p>
                Kontakt:<br/>
                E-mail: kontakt@pobiegam.net
            </p>
            <p>Operatorem strony internetowej Pobiegam.NET jest Administracja.</p>
            <p>Polityka prywatności określa zasady przechowywania i dostępu do informacji na urządzeniach Użytkownika za pomocą plików Cookies, służących realizacji usług świadczonych drogą elektroniczną przez Administrację.</p>
 
            Podstawy prawne przetwarzania danych osobowych:
            <ul>
                <li>Ustawa z dnia 29 sierpnia 1997 r. o ochronie danych osobowych (Dz. U. 2002 r. Nr 101 poz. 926 – tekst jednolity);</li>
                <li>Rozporządzenie Ministra Spraw Wewnętrznych i Administracji z dnia 29 kwietnia 2004 r. w sprawie dokumentacji przetwarzania danych osobowych oraz warunków technicznych i organizacyjnych, jakim powinny odpowiadać urządzenia i systemy informatyczne służące do przetwarzania danych osobowych (Dz. U. z dnia 1 maja 2004 r.);</li>
                <li>RODO - Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych).</li>
            </ul>

            <ol class="list-reg list-style-none">
                <li class="margin-li"><h3>§1. Definicje</h3>
                    <ol>
                        <li><strong>Administrator</strong> - obowiązuje definicja z Regulaminu.</li>
                        <li><strong>Cookies</strong> - oznacza dane informatyczne, w szczególności niewielkie pliki tekstowe, zapisywane i przechowywane na urządzeniach za pośrednictwem których Użytkownik korzysta ze stron internetowych Serwisu.</li>
                        <li><strong>Cookies Administratora</strong> - oznacza Cookies zamieszczane przez Administratora.</li>
                        <li><strong>Cookies Zewnętrzne</strong> - oznacza Cookies zamieszczane przez partnerów Administratora, za pośrednictwem strony internetowej Serwisu.</li>
                        <li><strong>Serwis</strong> - obowiązuje definicja z Regulaminu.</li>
                        <li><strong>Urządzenie</strong> - oznacza elektroniczne urządzenie za pośrednictwem, którego Użytkownik uzyskuje dostęp do Serwisu.</li>
                        <li><strong>Użytkownik</strong> - obowiązuje definicja z Regulaminu.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§2. Rodzaje wykorzystywanych Cookies</h3>
                    <ol>
                        <li>Stosowane przez Administratora Cookies są bezpieczne dla Urządzenia Użytkownika. W szczególności tą drogą nie jest możliwe przedostanie się do Urządzeń Użytkowników wirusów lub innego niechcianego oprogramowania lub oprogramowania złośliwego. Pliki te pozwalają zidentyfikować oprogramowanie wykorzystywane przez Użytkownika i dostosować Serwis indywidualnie każdemu Użytkownikowi. Cookies zazwyczaj zawierają nazwę domeny z której pochodzą, czas przechowywania ich na Urządzeniu oraz przypisaną wartość.</li>
                        <li>Administrator wykorzystuje dwa typy plików Cookies:
                            <ul>
                                <li>Cookies sesyjne: są przechowywane na Urządzeniu Użytkownika i pozostają tam do momentu zakończenia sesji danej przeglądarki. Zapisane informacje są wówczas trwale usuwane z pamięci Urządzenia. Mechanizm Cookies sesyjnych nie pozwala na pobieranie jakichkolwiek danych osobowych ani żadnych informacji poufnych z Urządzenia Użytkownika;</li>
                                <li>Cookies trwałe: są przechowywane na Urządzeniu Użytkownika i pozostają tam do momentu ich skasowania. Zakończenie sesji danej przeglądarki lub wyłączenie Urządzenia nie powoduje ich usunięcia z Urządzenia Użytkownika. Mechanizm Cookies trwałych nie pozwala na pobieranie jakichkolwiek danych osobowych ani żadnych informacji poufnych z Urządzenia Użytkownika.</li>
                            </ul>
                        </li>
                        <li>Użytkownik ma możliwość ograniczenia lub wyłączenia dostępu plików Cookies do swojego Urządzenia. W przypadku skorzystania z tej opcji korzystanie ze Serwisu będzie możliwe, poza funkcjami, które ze swojej natury wymagają plików Cookies.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§3. Cele w jakich wykorzystywane są Cookies</h3>
                    <ol>
                        <li>Administrator wykorzystuje Cookies Własne w następujących celach:
                            <ol type="a">
                                <li>konfiguracji Serwisu - dostosowania zawartości stron internetowych Serwisu do preferencji Użytkownika oraz optymalizacji korzystania ze stron internetowych Serwisu;</li>
                                <li>analiz i badań oraz audytu oglądalności – zbierania ogólnych i anonimowych danych statycznych, tworzenia anonimowych statystyk za pośrednictwem narzędzi analitycznych, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu korzystają ze stron internetowych Serwisu, co umożliwia ulepszanie ich struktury i zawartości;</li>
                                <li>świadczenia usług reklamowych – prezentowania przekazów reklamowych dostosowanych do preferencji Użytkownika.</li>
                            </ol>
                        </li>
                        <li>Administrator usługi wykorzystuje Cookies Zewnętrzne w następujących celach:
                            <ol type="a">
                                <li>konfiguracji Serwisu - dostosowania zawartości stron internetowych Serwisu do preferencji Użytkownika oraz optymalizacji korzystania z Serwisu (Administracja);</li>
                                <li>analiz i badań oraz audytu oglądalności - zbierania ogólnych i anonimowych danych statycznych za pośrednictwem narzędzi analitycznych Google Analitycs – administrator Cookies: Google Inc. z/s w USA;</li>
                                <li>świadczenia usług reklamowych – prezentowania przekazów reklamowych dostosowanych do preferencji Użytkownika z wykorzystaniem narzędzi analitycznych Google Adwords – administrator Cookies: Google Inc. z/s w USA, Adpilot Sp. z o.o. z/s w Warszawie.</li>
                            </ol>
                        </li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§4. Możliwości określenia warunków przechowywania lub uzyskiwania dostępu przez Cookies</h3>
                    <ol>
                        <li>Użytkownik może samodzielnie i w każdym czasie zmienić ustawienia dotyczące plików Cookies, określając warunki ich przechowywania i uzyskiwania dostępu przez pliki Cookies do Urządzenia Użytkownika. Zmiany ustawień, o których mowa w zdaniu poprzednim, Użytkownik może dokonać za pomocą ustawień przeglądarki internetowej lub za pomocą konfiguracji usługi. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików Cookies w ustawieniach przeglądarki internetowej bądź informować o ich każdorazowym zamieszczeniu Cookies na urządzeniu Użytkownika. Szczegółowe informacje o możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).</li>
                        <li>Użytkownik może w każdej chwili usunąć pliki Cookies korzystając z dostępnych funkcji w przeglądarce internetowej, której używa.</li>
                        <li>Ograniczenie stosowania plików Cookies, może wpłynąć na niektóre funkcjonalności dostępne w Serwisie.</li>
                    </ol>
                </li>
                
                <li class="margin-li"><h3>§5. Prawa użytkownika</h3>
                    <ol>
                        <li>Użytkownikowi serwisu przysługują m.in. prawa do:
                            <ol type="a">
                                <li>informacji o danych zgromadzonych odnośnie siebie (art. 15 RODO);</li>
                                <li>niezwłocznego sprostowania lub uzupełnienia danych osobowych (art. 16 RODO);</li>
                                <li>żądania usunięcia swoich danych osobowych (art. 17 RODO);</li>
                                <li>ograniczenia przetwarzania Twoich danych osobowych (art. 18 RODO);</li>
                                <li>przeniesienia danych (art. 20 RODO);</li>
                                <li>sprzeciwu wobec przetwarzania (art. 21 RODO);</li>
                                <li>złożenia skargi do organu nadzorczego.</li>
                            </ol>
                        </li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§6. Zmiana przepisów</h3>
                    <ol>
                        <li>Administracja zastrzega prawo do odpowiedniego dostosowania zapisów niniejszej polityki prywatności w przypadku zmiany wymogów ustawowych lub wprowadzania zmian w serwisie. O zmianach użytkownik zostaje powiadomiony drogą elektroniczną.</li>
                    </ol>
                </li>

                <li class="margin-li"><h3>§7. Przepisy końcowe</h3>
                    <ol>
                        <li>W przypadku pytań, uwag oraz wniosków dotyczących polityki ochrony prywatności i plików Cookies, adresem właściwym do kontaktu jest: kontakt@pobiegam.net .</li>
                    </ol>
                </li>
                
                
            </ol>

        </div>


    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>
