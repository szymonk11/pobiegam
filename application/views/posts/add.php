
<div class="main main-raised">
    <div id="content" class="container">
        <div class="section text-center">
            <h2 class="title">Dodaj ogłoszenie</h2>

            <div class="card text-left">
                <form class="pobiegam-form" method="POST" action="<?= base_url('add'); ?>">
                    <div class="card-body">

                        <?php if(isset($errors) && !empty($errors)): ?>
                        <div class="alert alert-danger" role="alert">Coś poszło nie tak... Sprawdź poprawność wprowadzonych danych.</div>
                        <?php endif; ?>

                        <h4 class="title">Co robisz?</h4>
                        <div class="has-success">
                            <select name="type" class="pobiegam-form-control form-control selectpicker" data-style="btn btn-link" id="name" value="<?= set_value('type'); ?>">
                                <?php $type = set_value('type'); foreach(get_all_sports() as $id => $sport): ?>
                                <option value="<?= $id; ?>" <?= ($id == $type)?'selected':''; ?>><?= $sport['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('type', '<p class="text-danger font-weight-normal">','</p>'); ?>
                        </div>
                
                        <h4 class="title">Na jakim poziomie?</h4>
                        <div style="margin-bottom:50px;">
                            <select name="level" class="form-control" id="post-level" value="<?= set_value('level'); ?>">
                                <?php $level = set_value('level'); ?>
                                <option value="1" <?= ($level==1)?'selected':''; ?>>Amator</option>
                                <option value="2" <?= ($level==2)?'selected':''; ?>>Średniozaawansowany</option>
                                <option value="3" <?= ($level==3)?'selected':''; ?>>Zaawansowany</option>
                                <option value="4" <?= ($level==4)?'selected':''; ?>>Ekspert</option>
                            </select>
                        </div>
                        <?= form_error('level', '<p class="text-danger font-weight-normal">','</p>'); ?>

                        <h4 class="title">Gdzie ćwiczysz?</h4>
                        <div class="has-success">
                            <input type="text" class="pobiegam-form-control form-control" id="icity" name="city" placeholder="Wpisz miejscowość" value="<?= set_value('city'); ?>" />
                            <?= form_error('city', '<p class="text-danger font-weight-normal">','</p>'); ?>
                        </div>

                        <h4 class="title">Kiedy działasz?</h4>
                        <div class="has-success">
                            Zawsze w:<br />
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Poniedziałki" <?= isset($_POST['day'])&&in_array('Poniedziałki',$_POST['day'])?'checked':''; ?> /> Poniedziałki
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Wtorki" <?= isset($_POST['day'])&&in_array('Wtorki',$_POST['day'])?'checked':''; ?> /> Wtorki
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Środy" <?= isset($_POST['day'])&&in_array('Środy',$_POST['day'])?'checked':''; ?> /> Środy
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Czwartki" <?= isset($_POST['day'])&&in_array('Czwartki',$_POST['day'])?'checked':''; ?> /> Czwartki
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Piątki" <?= isset($_POST['day'])&&in_array('Piątki',$_POST['day'])?'checked':''; ?> /> Piątki
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Soboty" <?= isset($_POST['day'])&&in_array('Soboty',$_POST['day'])?'checked':''; ?> /> Soboty
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="day[]" type="checkbox" value="Niedziele" <?= isset($_POST['day'])&&in_array('Niedziele',$_POST['day'])?'checked':''; ?> /> Niedziele
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <br />lub tylko dnia:<br />
                            <div class="has-success">
                                <input id="datepicker" name="date" type="date" class="pobiegam-form-control form-control" placeholder="DD.MM.RRRR" value="<?= set_value('date'); ?>" />
                            </div>
                            <?php if(isset($date_error)): ?><p class="text-danger font-weight-normal"><?= $date_error; ?></p><?php endif; ?>
                            
                            <br /><br />W godzinach:<br />
                            <div class="has-success">
                                <input id="itimefrom" type="time" name="time_from" class="pobiegam-form-control form-control" placeholder="HH:MM" style="display:inline-block;width:80px;" value="<?= set_value('time_from'); ?>" /> - 
                                <input id="itimeto" type="time" name="time_to" class="pobiegam-form-control form-control" placeholder="HH:MM" style="display:inline-block;width:80px;" value="<?= set_value('time_to'); ?>" />
                            </div>
                            <p class="text-danger font-weight-normal">
                                <?= form_error('time_from', '<span>','</span><br />'); ?>
                                <?= form_error('time_to', '<span>','</span>'); ?>
                            </p>
                        </div>

                        <h4 class="title">Kim jesteś?</h4>
                        <div class="has-success">
                            Imię:
                            <input type="text" name="name" class="pobiegam-form-control form-control" value="<?= set_value('name'); ?>" />
                            <?= form_error('name', '<p class="text-danger font-weight-normal">','</p>'); ?>
                        </div>
                        <br />
                        <div class="has-success">
                            Wiek:
                            <input type="number" name="age" class="pobiegam-form-control form-control" value="<?= set_value('age'); ?>" />
                            <?= form_error('age', '<p class="text-danger font-weight-normal">','</p>'); ?>
                        </div>

                        <h4 class="title">Jak się z Tobą skontaktować?</h4>
                        <div class="has-success">
                            E-mail:
                            <input type="email" name="email" class="pobiegam-form-control form-control" required="" value="<?= set_value('email'); ?>" />
                            <p class="text-muted">E-mail jest niezbędny do zarządzania ogłoszeniem.</p>
                            <?= form_error('email', '<p class="text-danger font-weight-normal">','</p>'); ?>
                            Telefon:
                            <input type="text" name="telephone" class="pobiegam-form-control form-control" value="<?= set_value('telephone'); ?>" />
                        </div>

                        <h4 class="title">Co jeszcze musimy wiedzieć?</h4>
                        <div class="has-success">
                            <textarea rows="5" maxlength="512" name="description" class="pobiegam-form-control form-control"><?= set_value('description'); ?></textarea>
                        </div>

                        <h4 class="title">Akceptujesz nasz regulamin?</h4>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input name="reg" class="form-check-input" type="checkbox" value="1" required="" />
                                * Oświadczam, że przeczytałem i zgadzam się z 
                                <span class="form-check-sign"><span class="check"></span></span>
                            </label>&nbsp;<a href="<?= base_url('regulamin#content'); ?>" target="_blank">regulaminem serwisu</a>.
                            <?= form_error('reg', '<p class="text-danger">','</p>'); ?>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input name="marketing" class="form-check-input" type="checkbox" value="1" />
                                Wyrażam zgodę na używanie środków komunikacji elektronicznej oraz telekomunikacyjnych urządzeń końcowych w celu przesyłania mi informacji handlowych oraz prowadzenia marketingu (np. newsletter, wiadomości SMS) przez Pobiegam.NET, podmioty powiązane i partnerów biznesowych.
                                <span class="form-check-sign"><span class="check"></span></span>
                            </label>
                        </div>

                        <h4 class="title">Potwierdź swoją tożsamość</h4>
                        <div class="g-recaptcha" data-sitekey="6LcBsDIUAAAAAPSPA1xJQSSLca-lMbS8QL-fiwM8"></div>
                        <?php if(isset($recaptcha_error)): ?><p class="text-danger font-weight-normal"><?= $recaptcha_error; ?></p><?php endif; ?>
                    </div>

                    <div class="card-body text-center">
                        <button class="btn btn-success btn-pobiegam">Dodaj ogłoszenie<div class="ripple-container"></div></button>
                    </div>

                </form>
            </div>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>

<?php 
    TemplateManager::add_javascript(array(
        'web/js/jquery.barrating.min.js',
        'web/js/plugins/jquery.mask.min.js',
        'web/js/views/posts.add.js'
    )); 
?>
