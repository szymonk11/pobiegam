
<div class="main main-raised">
    <div id="content" class="container">
        <div class="section text-center">
            <h2 class="title">Usuwanie ogłoszenia</h2>

            <?php $this->load->view('layout/alerts'); ?>

            <?php if(!isset($message_error)): ?>

            <div class="card text-left">
                <form class="pobiegam-form" method="POST" action="<?= base_url('posts/delete?unikey='.$unikey.'&email='.$email); ?>">
                    <input type="hidden" name="confirm" value="1" />
                    <div class="card-body">
                        <h4 class="text-center">Czy na pewno chcesz usunąć to ogłoszenie?</h4>
                    </div>
                    <div class="card-body text-center">
                        <button class="btn btn-success btn-pobiegam">Tak, usuń<div class="ripple-container"></div></button>
                        <a href="<?= base_url(); ?>" class="btn btn-secondary">Nie, wróć<div class="ripple-container"></div></a>
                    </div>
                </form>
            </div>

            <?php endif; ?>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>
   
</div>
