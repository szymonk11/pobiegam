
<div class="main main-raised">
    <div class="container">
        <div class="section">

            <div class="card">
                <div class="post-details card-body font-weight-normal">
                    <img class="post-details-img card-img-top" src="<?= base_url('web/img/types/'.get_img_name_by_type($post->type)); ?>" alt="<?= get_name_by_type($post->type); ?>" />
                    <div>
                        <h5 class="text-right icons-text-bottom font-weight-normal">
                            <i class="material-icons">location_on</i> <?= $post->city; ?>&nbsp;&nbsp;
                            <i class="material-icons">calendar_today</i> <?= $post->date; ?>&nbsp;&nbsp;
                            <i class="material-icons">access_time</i> <?= $post->time; ?>
                        </h5>
                        <h4 class="card-title"><?= $post->name; ?> (<?= $post->age; ?>)</h4>
                        <p id="post-contact">
                            <?= (!empty($post->telephone)) ? 'Tel.: '.$post->telephone.'<br />' : ''; ?>
                            <?= (!empty($post->gg)) ? 'GG: '.$post->gg.'<br />' : ''; ?>
                            <?= (!empty($post->email)) ? 'E-mail: '.$post->email.'<br />' : ''; ?>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                    <select id="post-level">
                        <option value="1" <?= ($post->level == 1) ? 'selected':''; ?>>Amator</option>
                        <option value="2" <?= ($post->level == 2) ? 'selected':''; ?>>Średniozaawansowany</option>
                        <option value="3" <?= ($post->level == 3) ? 'selected':''; ?>>Zaawansowany</option>
                        <option value="4" <?= ($post->level == 4) ? 'selected':''; ?>>Ekspert</option>
                    </select>
                </div>
                <div class="card-body"></div>
                <div class="card-body">
                    <p class="card-text font-weight-normal"><?= $post->desc; ?></p>
                </div>

                <div class="card-body text-center">
                    
                </div>

                <div class="card-body text-right">
                    <div style="display:inline-block; vertical-align: middle;" class="card-link fb-share-button" data-href="http://<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>&amp;src=sdkpreparse">Udostępnij</a></div>
                    <a type="button" href="" class="text-muted card-link" data-toggle="modal" data-target="#reportModal">Zgłoś post</a>
                </div>
            </div>

            <div class="text-center">
                <a href="<?= $back_url; ?>"><button class="btn btn-sm btn-success btn-pobiegam">Wróć<div class="ripple-container"></div></button></a>
            </div>

        </div>

       

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>

<!-- Modal -->
<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reportModalLabel">Zgłoś post</h5>
            </div>
            <div class="modal-body has-success">
                <p>Podaj powód:</p>
                <textarea id="reason" class="form-control" name="reason" rows="4" maxlength="512" style="width: 100%;" placeholder="Max 512 znaków"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                <button id="send-report" type="button" class="btn btn-success" data-id="<?= $post->id; ?>" data-href="<?= base_url('/report/'.$post->id) ?>">Zgłoś</button>
            </div>
        </div>
    </div>
</div>


<?php 
    TemplateManager::add_javascript(array(
        'web/js/jquery.barrating.min.js',
        'web/js/views/posts.post.js',
        'web/js/fb.js'
    )); 
?>