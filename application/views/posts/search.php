
<div class="main main-raised">
    <div class="container">
        <div class="section text-center">

            <?php $this->load->view('layout/alerts'); ?>

            <h2 class="title">Najnowsze ogłoszenia</h2>

            <?php if(empty($posts)): ?>
            <div class="alert alert-info" role="alert">
                Nie znaleźliśmy żadnych ogłoszeń spełniających podane kryteria :(
            </div>
            <?php else: ?>

            <div class="table-responsive">
                <table class="table-posts table table-hover">

                    <tbody>
                        <?php foreach($posts as $post): ?>
                        <tr>
                            <td><img src="<?= base_url('web/img/types/'.get_img_name_by_type($post->type)); ?>" style="height: 50px;" alt="" /></td>
                            <td><i class="material-icons">location_on</i> <?= $post->city; ?></td>
                            <td class="d-none d-sm-table-cell"><i class="material-icons">calendar_today</i> <?= $post->date; ?></td>
                            <td><i class="material-icons">access_time</i> <?= $post->time; ?></td>
                            <td><a href="<?= get_post_url($post); ?>"><button class="btn btn-sm btn-success btn-pobiegam">Więcej<div class="ripple-container"></div></button></a></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>

            <?php if($max_pages > 1): ?>
            <nav aria-label="">
                <ul class="pobiegam-pagination pagination justify-content-center">
                    <?php $url = base_url(); ?>    
                
                    <li class="page-item <?=($active_page>1)?'':'disabled';?>"><a class="page-link" href="<?= $url.'search?page='.($active_page-1).'&type='.$search_data['type'].'&city='.$search_data['city']; ?>">&lt;</a></li>
                    
                    <?php for($i=1; $i <= $max_pages; $i++): ?>
                    <?php if($i==$active_page): ?>
                        <li class="page-item active"><a class="page-link" href="<?= $url.'search?page='.$i.'&type='.$search_data['type'].'&city='.$search_data['city']; ?>"><?= $i; ?> <span class="sr-only">(current)</span></a></li>
                    <?php else: ?>
                        <li class="page-item"><a class="page-link" href="<?= $url.'search?page='.$i.'&type='.$search_data['type'].'&city='.$search_data['city']; ?>"><?= $i; ?></a></li>
                    <?php endif; ?>
                    <?php endfor; ?>

                    <li class="page-item <?=($active_page==$max_pages )?'disabled':'';?>"><a class="page-link" href="<?= $url.'search?page='.($active_page+1).'&type='.$search_data['type'].'&city='.$search_data['city']; ?>">&gt;</a></li>
                </ul>
            </nav>
            <?php endif; ?>

            <?php endif; ?>

        </div>

    </div>

    <div class="container">
        <div class="motto">Pierwsze takie miejsce w sieci, łączące ludzi i sport!<br>Kreujemy zdrowy tryb życia w radosnym towarzystwie!</div>
    </div>

    
</div>
