jQuery(document).ready(function($) {
	
	//SCROLL UP
	$('#scroll_top').on('click', (e) => {
		e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#top').offset().top
        }, 1000, function()
        {
			return;
        });
	});
	
	// NOTIFY, POWIADOMIENIA
	$.notify.addStyle('pobiegam', {
		html: "<div><span data-notify-text/></div>",
		classes: {
			base: {
				"color": "#fff",
				"font-size": "16px",
				"padding": "10px 15px 10px 15px",
				"background-color": "#24262b",
				"border": "1px solid #24262b",
				"border-radius": "10px",
				"white-space": "nowrap",
				"padding-right": "25px",
				"box-shadow": "0px 0px 2px 0px #666"
			},
			succes: {
				"background-color": "#7dc246",
				"border": "1px solid #7dc246"
			},
			error: {
				"background-color": "#ee6e73",
				"border": "1px solid #ee6e73"
			}
		}
	});
	
	$.notify.defaults({
		clickToHide: true,
		autoHide: true,
		autoHideDelay: 4000,
		position: 'right middle',
		className: 'base',
		style: 'pobiegam'
		
	});
	// ================================

	//TOP SEARCH
	$('#TopSearchButton').on("click", function() {
		let url = $('#TopSearchButton').attr('data-url');

		let type = $('#TopSearchType').val();
		if(!type) {
			type = 0;
		}
		
		let city = $('#TopSearchCity').val();
		if(!city) {
			city = 0;
		}
		
		location.href = url+"?type="+type+"&city="+city+"";
	});
	
});