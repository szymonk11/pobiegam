jQuery(document).ready(() => {

    $('#insert-button').on("click", function(ev) {
        ev.preventDefault();

        var name = $('#contactform #iname').val();
        if(!name || name.length < 3 || name == "") {
            $.notify("Podaj swoje imię (3)");
            return;
        }
                
        var email = $('#contactform #imail').val();
        var reg_mail = /^([a-zA-Z0-9])+([.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-]+)+/;
        if(!email || email == "" || reg_mail.test(email) == false) {
            $.notify("Podaj poprawny adres e-mail");
            return;
        }
        var desc = $('#contactform #idesc').val();
        if(!desc || desc == "" || desc.length < 15) {
            $.notify("Napisz coś (15)");
            return;
        }
        
        $.notify("Trwa wysyłanie. Proszę czekać...");
        $('#insert-button').attr('disabled', 'true');
            
        $.ajax({
            method: 'POST',
            url: $('#contactform').attr('action'),
            type: 'POST',
            data: { 
                name: name,
                email: email,
                text: desc,
                recaptcha: grecaptcha.getResponse()
            },
            dataType: 'json'
        })
        .done(function( msg ) {
            if(msg.status == "success") {
                $('#cont').html('<h2 class="text-center">Wiadomość została wysłana.</h2><div class="text-center font-500">Śledz swoją skrzynkę pocztową, wkrótce ktoś od nas odpisze na Twoją wiadomość.<br>Dziękujemy i pozdrawiamy, ekipa Pobiegam.NET!</div>');
                $.notify("Wiadomość wysłana", "succes");
                //goToIndex(5);
            } else {
                $.notify("Wystąpił nieoczekiwany błąd", "error");
                //alert("Wystąpił nieoczekiwany błąd.");
            }

            $('#insert-button').removeAttr('disabled', 'true');
        })
        .fail(function() {
            $.notify("Wystąpił nieoczekiwany błąd", "error");

            $('#insert-button').removeAttr('disabled', 'true');
        });
    });

});
