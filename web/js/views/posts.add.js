jQuery(document).ready(($) => {
    $('#post-level').barrating({
        theme: 'bars-movie',
        readonly: false,
        showSelectedRating: true
    });

    /*$('#datepicker').mask('00.00.0000', {
        placeholder: 'DD.MM.RRRR'
    });*/

    $('#itimefrom,#itimeto').mask('00:00', {
        placeholder: 'HH:MM'
    });

    /*$("#datepicker").datepicker({
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        minDate: 0,
        maxDate: "+2m",
        monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
        dayNamesMin: ["Nd", "Pon", "Wt", "Śr", "Czw", "Pt", "Sob"],
        defaultDate: 1
    });
    
    $('#itimefrom,#itimeto').timepicker({
        timeFormat: 'HH:mm',
        interval: 30,
        minTime: '00:00',
        maxTime: '23:59',
        defaultTime: 'now',
        startTime: 'now',
        dynamic: true,
        dropdown: false,
        scrollbar: false
    });*/

    $('input[name="day[]"]').click(function() {
        var exists = false;
        
        $('input[name="day[]"]').each(function(index) {
            if($(this).prop('checked') == true) {
                exists = true;
            }
        });
        
        if(exists == true) {
            $('#datepicker').val("");
            $('#datepicker').prop('disabled', true);
        } else {
            $('#datepicker').prop('disabled', false);
        }
    });

});