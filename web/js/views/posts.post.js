jQuery(document).ready(function($) {
    var frezzeReport = false;

    //http://antenna.io/demo/jquery-bar-rating/examples/
    $('#post-level').barrating({
            theme: 'bars-movie',
            readonly: true,
            showSelectedRating: true
    });

    $('#reportModal #send-report').on('click', function(e) {
        e.preventDefault();
        if(frezzeReport == true) {
                $.notify("Odczekaj chwilę...");
        } else {
            var id = parseInt($(this).data('id'));
            if(id > 0) {
                var reason = $('#reportModal #reason').val();
                if(reason.length < 3) {
                    $.notify("Musisz podać powód!");
                    return;
                }
                $.ajax({
                    method: 'POST',
                    url: $(this).data('href'),
                    dataType: 'json',
                    data: { id: id, reason: reason }
                })
                .done(function( msg ) {
                    if(msg.status == 1) {
                        $('#reportModal #reason').val('');
                        $('#reportModal').modal('hide');
                        $.notify("Zgłoszenie wysłane. Dziękujemy!", "succes");
                        frezzeReport = true;
                        setTimeout(function() { frezzeReport = false; }, 1000*60*2);
                    } else {
                        $.notify("Nie udało się zgłosić", "error");
                    }
                })
                .fail(function() {
                    $.notify("Nie udało się zgłosić", "error");
                });
            } else {
                $.notify("Wystąpił nieoczekiwany błąd", "error");
            }
        }
    });
});